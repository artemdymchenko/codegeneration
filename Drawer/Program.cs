﻿using System.Collections.Generic;

namespace CodeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {            
            //basic initialization----------------------------------------------
            var generator = new CodeGenerator();
            var compiler = new Compiler();
            var xmlParser = new XmlParser(@"D:\Study2\Diploma\ClientProjectExample\XML.xml");

            var destinationFolder = xmlParser.DestinationFolder;
            var namespaceName = xmlParser.NamespaceName;

            //generation of the code
            generator.GenerateEnvironmentClassSourceFile(destinationFolder, namespaceName, xmlParser.EnvironmentAttributes);
            generator.GenerateBaseSensor(destinationFolder, namespaceName);
            generator.GenerateProtocolClassesSourceFiles(destinationFolder, namespaceName, xmlParser.Protocols);
            generator.GenerateStateSourceFiles(destinationFolder, namespaceName, xmlParser.States, xmlParser.AgentRoles);
            generator.GenerateBeliefsInterfaceSourceFiles(destinationFolder, namespaceName, xmlParser.Beliefs);
            generator.GeneratePlanClassSourceFiles(destinationFolder, namespaceName, xmlParser);
            generator.GenerateGoalClassSourceFiles(destinationFolder, namespaceName, xmlParser.Plans, xmlParser.Goals);
            generator.GenerateBaseAgentSourceFile(destinationFolder, namespaceName);
            generator.GenerateBlackBoardClassSourceFile(destinationFolder, namespaceName);
            generator.GenerateAgentSourceFiles(destinationFolder, namespaceName, xmlParser);

            //compiling---------------------------------------------------------
            var usings = new List<string>
            {
                "using System.Collections.Generic;\n",
                "using System.ComponentModel;\n",
                "using System.Runtime.CompilerServices;\n",
                "using System;\n",
                "using System.Linq;\n",
            };

            compiler.Compile(destinationFolder + namespaceName + ".dll", generator.GeneratedFiles, usings);
        }
    }
}
