﻿using System.Collections.Generic;

namespace CodeGenerator.XMLEntities
{
    public class XMLAgentRole
    {
        public string Name { get; private set; }
        public string TargetState { get; private set; }
        public string TargetBelief { get; private set; }
        public List<string> Protocols { get; private set; }

        public XMLAgentRole(string name, string targetState, string targetBelief, List<string> protocols)
        {
            Name = name;
            TargetState = targetState;
            TargetBelief = targetBelief;
            Protocols = protocols;
        }
    }
}