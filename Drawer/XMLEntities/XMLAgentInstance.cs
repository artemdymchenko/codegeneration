﻿using System.Collections.Generic;
using System.Net;

namespace CodeGenerator.XMLEntities
{
    public class XMLAgentInstance
    {
        public string Name { get; private set; }
        public string Role { get; private set; }
        public List<XMLSensor> Sensors { get; private set; }

        public XMLAgentInstance(string name, string role, List<XMLSensor> sensors)
        {
            Role = role;
            Name = name;
            Sensors = sensors;
        }
    }
}