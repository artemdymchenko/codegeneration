﻿namespace CodeGenerator.XMLEntities
{
    public class XMLPlan
    {
        public XMLPlan(string planName, string targetGoal, string executePlanScriptDestination, string utilityScriptDestination)
        {
            PlanName = planName;
            TargetGoal = targetGoal;
            ExecutePlanScriptDestination = executePlanScriptDestination;
            UtilityScriptDestination = utilityScriptDestination;
        }

        public string PlanName { get; private set; }
        public string TargetGoal { get; private set; }
        public string UtilityScriptDestination { get; private set; }
        public string ExecutePlanScriptDestination { get; private set; }
    }
}