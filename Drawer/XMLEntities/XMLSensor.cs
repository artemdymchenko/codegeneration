﻿namespace CodeGenerator.XMLEntities
{
    public class XMLSensor
    {
        public XMLSensor(string logicStcriptPath, string attributeName, string name)
        {
            LogicStcriptPath = logicStcriptPath;
            AttributeName = attributeName;
            Name = name;
        }

        public string LogicStcriptPath { get; private set; }
        public string AttributeName { get; private set; }
        public string Name { get; private set; }
    }
}