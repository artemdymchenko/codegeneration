﻿using System.Collections.Generic;

namespace CodeGenerator.XMLEntities
{
    public class XMLProtocol
    {
        public XMLProtocol(string name)
        {
            Name = name;
            //Source = source;
            Funcs = new List<XMLProtocolFunc>();
        }

        public string Name { get; set; }
        public List<XMLProtocolFunc> Funcs { get; set; }
        //public string Source { get; set; }
    }
}