﻿using System.Collections.Generic;

namespace CodeGenerator.XMLEntities
{
    public class XMLProtocolFunc
    {
        public string Name { get; set; }
        public string Source { get; set; }
        public List<string> ParameterTypes { get; set; }

        public XMLProtocolFunc(string name, string source, List<string> parameterTypes)
        {
            Name = name;
            Source = source;
            ParameterTypes = parameterTypes;
        }
    }
}