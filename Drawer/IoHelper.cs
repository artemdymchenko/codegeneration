﻿using System;
using System.IO;
using System.Text;

namespace CodeGenerator
{
    public class IoHelper
    {
        public static void GetFileTextToStringBuilder(StringBuilder mainSb, string path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    String line = sr.ReadToEnd();
                    mainSb.Append(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        } 
    }
}