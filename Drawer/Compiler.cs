﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using AgentsLibrary;
using Microsoft.CSharp;

namespace CodeGenerator
{
    public class Compiler
    {
        public string Test1()
        {
            return new GeneratedClass().HelloWorld();
        }

        public string Test2()
        {
            return new GeneratedClass2().HelloWorld();
        }

        public void Compile(string targetName, List<string> fileNames, List<string> usings)
        {
            CSharpCodeProvider csp = new CSharpCodeProvider();
            ICodeCompiler cc = csp.CreateCompiler();
            CompilerParameters cp = new CompilerParameters
            {
                OutputAssembly = targetName,
                CompilerOptions = "/target:library /optimize",
                GenerateExecutable = false,
                GenerateInMemory = false
            };
            cp.ReferencedAssemblies.Add("System.dll");
            cp.ReferencedAssemblies.Add("System.Core.dll");
            cp.ReferencedAssemblies.Add("System.Data.dll");
            cp.ReferencedAssemblies.Add("System.Xml.dll");
            //------------------------------------------------------------
            var usingSb = new StringBuilder();
            var sb = new StringBuilder();
            // Open the text file using a stream reader.
                foreach (string file in fileNames)
                {
                    try
                    {
                        using (StreamReader sr = new StreamReader(file))
                        {
                            // Read the stream to a string, and write the string to the console.
                            var line = sr.ReadToEnd();
                            var index = line.IndexOf("namespace", StringComparison.Ordinal);
                            usingSb.Append(line.Substring(0, index));
                            sb.Append(line.Substring(index));
                        }
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("The file could not be read:");
                        Console.WriteLine(e.Message);
                    }
                }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("The file could not be read:");
            //    Console.WriteLine(e.Message);
            //}
            //---------------------------------------------------------------
            //append all 'using' statements

            //var usings = usingSb.ToString().Split('\n');
            var distinctUsingsList = usings.Distinct().ToList();
            var distinctUsingsSb = new StringBuilder();
            foreach (var distinctUsing in distinctUsingsList)
            {
                distinctUsingsSb.Append(distinctUsing);
            }
            //sb.Insert(0, usingSb);
            sb.Insert(0, distinctUsingsSb);

            Console.WriteLine(sb.ToString());
            var cr = cc.CompileAssemblyFromSource(cp, sb.ToString());
  
            System.Collections.Specialized.StringCollection sc = cr.Output;
            foreach (string s in sc)
            {
                Console.WriteLine(s);
            }

            if (cr.Errors.Count > 0)
            {
                foreach (CompilerError ce in cr.Errors)
                {
                    Console.WriteLine(ce.ErrorNumber + ": " + ce.ErrorText);
                }
            }
        }
    }
}