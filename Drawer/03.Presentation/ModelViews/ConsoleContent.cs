﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Drawer.Application.Core;
using Drawer.DependencyInjection;

namespace Drawer.Views
{
    public class ConsoleContent : INotifyPropertyChanged
    {
        private const string InitialSymbol = ">";
        string _consoleInput = InitialSymbol;
        ObservableCollection<string> _consoleOutput = new ObservableCollection<string> { "Command console..." };

        public ConsoleContent()
        {         
        }

        public string ConsoleInput
        {
            get
            {
                return _consoleInput;
            }
            set
            {
                _consoleInput = value;
                OnPropertyChanged("ConsoleInput");
            }
        }

        public ObservableCollection<string> ConsoleOutput
        {
            get
            {
                return _consoleOutput;
            }
            set
            {
                _consoleOutput = value;
                OnPropertyChanged("ConsoleOutput");
            }
        }

        public void RunCommand(IResolver resolver)
        {
            ConsoleOutput.Add(ConsoleInput);
            var command = ConsoleInput.Split(' ');

            switch (command[0].Substring(1))
            {
                case "clr":
                {
                    ConsoleOutput.Clear();
                    break;
                }
                default:
                    ConsoleOutput.Add(resolver.ConsoleCommandParser.ParseCommand(command));
                    break;
            }
 
            ConsoleInput = InitialSymbol;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}