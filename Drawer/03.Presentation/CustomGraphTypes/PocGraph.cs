﻿using QuickGraph;

namespace Drawer.CustomTypes
{
    public class PocGraph : BidirectionalGraph<PocVertex, IEdge<PocVertex>>
    {
        public PocGraph() { }

        public PocGraph(bool allowParallelEdges)
            : base(allowParallelEdges) { }

        public PocGraph(bool allowParallelEdges, int vertexCapacity)
            : base(allowParallelEdges, vertexCapacity) { }
    }
}