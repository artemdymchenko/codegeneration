﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class PlanVertex : BaseVertex
    {
        private const string Name = "Plan";

        public PlanVertex(string name) : base(Name)
        {
            PlanName = name;
        } 
    }
}