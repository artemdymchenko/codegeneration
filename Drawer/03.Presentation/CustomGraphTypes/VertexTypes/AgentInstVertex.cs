﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class AgentInstVertex : BaseVertex
    {
        private const string Name = "AgentInst";

        public AgentInstVertex(string name)
            : base(Name)
        {
            AgentInstanceName = name;
        }
    }
}