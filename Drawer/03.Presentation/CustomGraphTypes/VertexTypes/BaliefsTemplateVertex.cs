﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class BaliefsTemplateVertex : BaseVertex
    {
        private static string Name = "beliefs";

        public BaliefsTemplateVertex(string beliefsTemplateName) : base(Name)
        {
            BeliefsTemplateName = beliefsTemplateName;
        }  
    }
}