﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class AgentRoleVertex : BaseVertex
    {
        private const string Name = "AgentRole";

        public AgentRoleVertex(string agentRoleName) : base(Name)
        {
            AgentRoleName = agentRoleName;
        } 
    }
}