﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class ProtocolVertex : BaseVertex
    {
        private const string Name = "Protocol";

        public ProtocolVertex(string name) : base(Name)
        {
            ProtocolName = name;
        } 
    }
}