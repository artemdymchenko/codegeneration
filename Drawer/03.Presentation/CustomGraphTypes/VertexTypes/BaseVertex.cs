﻿using System;
using System.Collections.Generic;

namespace Drawer.CustomTypes.VertexTypes
{
    enum EntityTypes
    {
        Actor, Database, Environment, Blackboard, beliefs, Goal, Plan, Protocol, Sensor
    }


    public abstract class BaseVertex
    {
        public string Label { get; set; }

        protected Dictionary<string, string> Attributes { get; set; }

        protected BaseVertex(string label)
        {
            BeliefsAttribs = new List<TypeName>();
            StateAttribs = new List<TypeName>();
            Label = label;
        }

        public override string ToString()
        {
            return Label;
        }

        //AgentRole
        public string AgentRoleName { get; set; }
        public Guid AgentRoleGuid { get; set; }

        //AgentInstance
        public string AgentInstanceName { get; set; }
        public Guid AgentInstanceGuid { get; set; }
        public Guid AgentInstanceRoleGuid { get; set; }

        //BeliefsTemplate
        public string BeliefsTemplateName { get; set; }
        public Guid AssociatedWithBeliefAgentGuid { get; set; }
        public List<TypeName> BeliefsAttribs { get; set; }

        //Environment attribute
        public string EnvironemntAttributeName { get; set; }
        public string EnvironemntAttributeType { get; set; }

        //GoalVertex
        public string GoalName { get; set; }
        public string AssociatedStateName { get; set; }
        //public List<Plan> AssociatedPlansNames { get; set; }
        //TODO: add script info

        //PlanVertex
        public string PlanName { get; set; }
        public string AssociatedAgentNameToThisPlan { get; set; }
        public string AssociatedGoalNameToThisPlan { get; set; }

        //Protocol
        public string ProtocolName { get; set; }
        public Guid AssociatedClientAgentGuidToThisProtocol { get; set; }
        public Guid AssociatedServerAgentGuidToThisProtocol { get; set; }

        //Sensor
        public string SensorName { get; set; }
        public Guid AssociatedAgentGuidToThisSensor { get; set; }
        public string AssociatedEnvironmentAttributeNameToThisSensor { get; set; }

        //State
        public string StateName { get; set; }
        public Guid AssociatedWithStateAgentGuid { get; set; }
        public List<TypeName> StateAttribs { get; set; }
    }

    public class TypeName
    {
        public string Type { get; set; }
        public string Name { get; set; }
    }

}