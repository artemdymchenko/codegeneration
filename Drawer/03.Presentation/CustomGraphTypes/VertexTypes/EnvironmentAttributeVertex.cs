﻿using System.Collections.Generic;

namespace Drawer.CustomTypes.VertexTypes
{
    public class EnvironmentAttributeVertex : BaseVertex
    {
        private const string Name = "_environment";

        public EnvironmentAttributeVertex() : base(Name)
        {
            Attributes = new Dictionary<string, string>();
        }
    }
}