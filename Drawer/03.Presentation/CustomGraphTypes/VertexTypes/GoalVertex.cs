﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class GoalVertex : BaseVertex
    {
        private const string Name = "GoalVertex";

        public GoalVertex(string name)
            : base(Name)
        {
            GoalName = name;
        } 
    }
}