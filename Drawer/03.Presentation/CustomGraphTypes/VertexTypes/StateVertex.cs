﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class StateVertex : BaseVertex
    {
        private const string Name = "State";

        public StateVertex(string name) : base(Name)
        {
            StateName = name;
        } 
    }
}