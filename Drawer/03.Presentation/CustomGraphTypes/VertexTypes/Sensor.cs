﻿namespace Drawer.CustomTypes.VertexTypes
{
    public class SensorVertex : BaseVertex
    {
        private const string Name = "Sensor";

        public SensorVertex(string name) : base(Name)
        {
            SensorName = name;
        } 
    }
}