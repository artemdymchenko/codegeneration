﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Drawer.Annotations;
using Drawer.DependencyInjection;
using Drawer.Views;

namespace Drawer
{
    /// <summary>
    /// Interaction logic for Console.xaml
    /// </summary>
    public partial class Console
    {
        readonly ConsoleContent _dc = new ConsoleContent();
        //public IDataInstance data { get; private set; }

        public Console()
        {
            InitializeComponent();
            //data = DataHolder.Instance;

            DataContext = _dc;
            //Loaded += MainWindow_Loaded;

            InputBlock.KeyDown += InputBlock_KeyDown;
            InputBlock.Focus();

        }

        void InputBlock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                _dc.ConsoleInput = InputBlock.Text;
                try
                {
                    _dc.RunCommand(Resolver.Instance);
                }
                catch (Exception ex)
                {
                    throw new Exception("Command exception " + ex.Message);
                }
  
                InputBlock.Focus();
                InputBlock.CaretIndex = 1;
                Scroller.ScrollToBottom();
            }
        }
    }
}
