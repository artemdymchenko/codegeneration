﻿using System;
using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class Protocol : BaseEntity
    {
        private string _name;
        private Guid _client;
        private Guid _server;
        public List<ProtocolMethod> ProtocolMethods { get; private set; }

        //For serialization
        public Protocol()
        {
            ProtocolMethods = new List<ProtocolMethod>();
        }

        public Protocol(string name)
        {
            Name = name;
            Client = Guid.Empty;
            Server = Guid.Empty;
            ProtocolMethods = new List<ProtocolMethod>();
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public Guid Client
        {
            get { return _client; }
            set
            {
                if (value.Equals(_client)) return;
                _client = value;
                OnPropertyChanged();
            }
        }

        public Guid Server
        {
            get { return _server; }
            set
            {
                if (value.Equals(_server)) return;
                _server = value;
                OnPropertyChanged();
            }
        }
    }

    public class ProtocolComparer : IEqualityComparer<Protocol>
    {
        public bool Equals(Protocol x, Protocol y)
        {
            return x.Name == y.Name && x.Client == y.Client && x.Server == y.Server;
        }

        public int GetHashCode(Protocol obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Name.GetHashCode() ^ obj.Client.GetHashCode() ^ obj.Server.GetHashCode();
        }
    }
}