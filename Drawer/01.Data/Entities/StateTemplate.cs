﻿using System;
using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class StateTemplate : BaseTypeNameDictionary
    {
        //For serialization
        public StateTemplate()
        {
        }

        public StateTemplate(string name)
        {
            _name = name;
        }

        private Guid _associatedAgentRoleGuid;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public Guid AssociatedAgentRoleGuid
        {
            get { return _associatedAgentRoleGuid; }
            set
            {
                if (value.Equals(_associatedAgentRoleGuid)) return;
                _associatedAgentRoleGuid = value;
                OnPropertyChanged();
            }
        }

        private string _name;
    }

    public class StateTemplateComparer : IEqualityComparer<StateTemplate>
    {
        public bool Equals(StateTemplate x, StateTemplate y)
        {
            return ((x.Name == y.Name) && (x.AssociatedAgentRoleGuid == y.AssociatedAgentRoleGuid) && x.Equals(y));
        }

        public int GetHashCode(StateTemplate obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Name.GetHashCode() ^ obj.AssociatedAgentRoleGuid.GetHashCode();
        }
    }
}