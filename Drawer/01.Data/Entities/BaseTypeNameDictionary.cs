﻿using System.Collections;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.CompilerServices;
using Drawer.Annotations;
using Drawer.Helper;

namespace Drawer.Data.Entities
{
    public class BaseTypeNameDictionary : BaseEntity
    {
        public SerializableDictionary<string, string> Attribs { get; set; }

        public void Add(string type, string name)
        {
            Attribs.Add(name, type);
            OnPropertyChanged();
        }

        public bool Remove(string name)
        {
            if (!ContainsKey(name))
                return false;

            Attribs.Remove(name);
            OnPropertyChanged();
            return true;
        }

        public bool ContainsKey(string name)
        {
            return Attribs.ContainsKey(name);
        }

        public void Clear()
        {
            Attribs.Clear();
            OnPropertyChanged();
        }

        public BaseTypeNameDictionary()
        {
            Attribs = new SerializableDictionary<string, string>();
        }

        public bool Equals(BaseTypeNameDictionary anotherDictionary)
        {
            if (Attribs.Count != anotherDictionary.Attribs.Count)
                return false;
            if (Attribs.Keys.Except(anotherDictionary.Attribs.Keys).Any())
                return false;
            if (anotherDictionary.Attribs.Keys.Except(Attribs.Keys).Any())
                return false;
            foreach (var pair in Attribs)
                if (pair.Value != anotherDictionary.Attribs[pair.Key])
                    return false;
            return true;
        }

    }
}