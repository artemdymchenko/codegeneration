﻿using System;
using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class Plan : BaseEntity
    {
        private Guid _associatedAgentGuid;
        private Goal _goal;
        private string _name;
        private string _executeScriptPath;
        //For serialization
        public Plan()
        {
        }

        public Plan(string name)
        {
            Name = name;
        }
        //public void ConnectToAgent(Guid id)
        //{
        //    AssociatedWithBeliefAgentGuid = id;
        //}
        public void ConnectToAGoal(Goal goal)
        {
            Goal = goal;
            Goal.ConnectToPlan(this);
        }
        public void DisconnectFromGoal(Goal goal)
        {
            Goal = null;
            goal.DisonnectFromPlan(this);
        }

        public Guid AssociatedAgentGuid
        {
            get { return _associatedAgentGuid; }
            set
            {
                if (value.Equals(_associatedAgentGuid)) return;
                _associatedAgentGuid = value;
                OnPropertyChanged();
            }
        }

        public Goal Goal
        {
            get { return _goal; }
            set
            {
                if (Equals(value, _goal)) return;
                _goal = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public string ExecuteScriptPath
        {
            get { return _executeScriptPath; }
            set
            {
                if (value == _executeScriptPath) return;
                _executeScriptPath = value; 
                OnPropertyChanged(); 
            }
        }
    }

    public class PlanComparer : IEqualityComparer<Plan>
    {
        public bool Equals(Plan x, Plan y)
        {
            if ((x.Goal == null) == (y.Goal != null))
                return false;

            var res = x.Name == y.Name && x.AssociatedAgentGuid == y.AssociatedAgentGuid;
            if (x.Goal != null && y.Goal != null)
                res = res && x.Goal.Name == y.Goal.Name;

            return res;
        }

        public int GetHashCode(Plan obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            var hash = obj.Name.GetHashCode() ^ obj.AssociatedAgentGuid.GetHashCode();
            if (obj.Goal != null)
                hash = hash ^ obj.Goal.Name.GetHashCode();

            return hash;
        }
    }
}