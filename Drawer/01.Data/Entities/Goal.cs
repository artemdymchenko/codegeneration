﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Drawer.Data.Entities
{
    public class Goal : BaseEntity
    {
        private string _name;
        private string _script;
        private readonly List<Plan> _plans;
        private string _associatedStateName;

        //For serialization
        public Goal()
        {
            _plans = new List<Plan>();
            AssociatedStateName = null;
            Name = "";
            Script = "";
        }

        public Goal(string name)
        {
            _plans = new List<Plan>();
            AssociatedStateName = null;
            Name = name;
            Script = "";
        }

        public void ConnectToPlan(Plan plan)
        {
            if (!_plans.Contains(plan))
            {
                _plans.Add(plan);
                if (plan.Goal == null || plan.Goal.Guid != Guid)
                    plan.ConnectToAGoal(this);
                OnPropertyChanged();
            }
        }

        public void DisonnectFromPlan(Plan plan)
        {
            var plansWithName = _plans.Select(x => x.Name == plan.Name);
            if (plansWithName.Count() > 1)
                throw new Exception("Several plans with the same name");

            if (plansWithName.Count() == 1)
            {
                _plans.Remove(plan);
                if (plan.Goal != null && plan.Goal.Guid == Guid)
                    plan.DisconnectFromGoal(this);
                OnPropertyChanged();
            }
        }

        public List<Plan> GetPlans()
        {
            return _plans;
        }

        public string Script
        {
            get { return _script; }
            set
            {
                if (value == _script) return;
                _script = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public string AssociatedStateName
        {
            get { return _associatedStateName; }
            set
            {
                if (value == _associatedStateName) return;
                _associatedStateName = value;
                OnPropertyChanged();
            }
        }
    }

    public class GoalComparer : IEqualityComparer<Goal>
    {
        public bool Equals(Goal x, Goal y)
        {
            return x.Name == y.Name && x.Script == y.Script && x.AssociatedStateName == y.AssociatedStateName
                && x.GetPlans().SequenceEqual(y.GetPlans(), new PlanComparer());
        }

        public int GetHashCode(Goal obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            var hash = obj.Name.GetHashCode() ^ obj.AssociatedStateName.GetHashCode() ^ obj.Script.GetHashCode();
            hash = obj.GetPlans().Select(x => x.Name.GetHashCode()).Aggregate(hash, (current, result) => current ^ result);
            return hash;
        }
    }
}