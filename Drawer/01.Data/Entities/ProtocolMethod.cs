﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drawer.Data.Entities
{
    public class ProtocolMethod
    {
        //For serialization
        public ProtocolMethod()
        {
            Types = new List<string>();
        }

        public ProtocolMethod(string methodName)
        {
            MethodName = methodName;
            Types = new List<string>();
            Source = "";
        }

        public string MethodName { get; set; }
        public string Source { get; set; }
        public List<string> Types { get; set; }
    }
}
