﻿using System;
using System.Collections.Generic;
using Drawer.Data.Entities;

namespace Drawer.Data.Entities
{
    public class BeliefsTemplate : BaseTypeNameDictionary
    {
        private string _name;
        private Guid _associatedAgentRoleGuid;

        //Empty constructor for proper serialization
        public BeliefsTemplate()
        {
        }

        public BeliefsTemplate(string name)
        {
            _name = name;
            _associatedAgentRoleGuid = Guid.Empty;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public Guid AssociatedAgentRoleGuid
        {
            get { return _associatedAgentRoleGuid; }
            set
            {
                if (value.Equals(_associatedAgentRoleGuid)) return;
                _associatedAgentRoleGuid = value;
                OnPropertyChanged();
            }
        }
    }

    class BeliefsTemplateComparer : IEqualityComparer<BeliefsTemplate>
    {
        public bool Equals(BeliefsTemplate x, BeliefsTemplate y)
        {
            return ((x.Name == y.Name) && (x.AssociatedAgentRoleGuid == y.AssociatedAgentRoleGuid) && x.Equals(y));
        }

        public int GetHashCode(BeliefsTemplate obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Name.GetHashCode() ^ obj.AssociatedAgentRoleGuid.GetHashCode();
        }
    }
}