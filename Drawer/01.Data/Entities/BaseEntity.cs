﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Drawer.Annotations;

namespace Drawer.Data.Entities
{
    public class BaseEntity : INotifyPropertyChanged
    {
        public Guid Guid { get; set; }

        public BaseEntity() 
        {
            Guid = Guid.NewGuid();
        }

        //public Guid Guid { get { return _guid; } set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}