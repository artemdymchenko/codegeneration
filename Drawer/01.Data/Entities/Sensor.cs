﻿using System;
using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class Sensor : BaseEntity
    {
        private string _script;
        private string _targetEnvironment;
        private Guid _associatedAgentInstanceGuid;
        private string _name;
        //For serialization
        public Sensor()
        {
        }

        public Sensor(string name)
        {
            Name = name;
        }

        public string Script
        {
            get { return _script; }
            set
            {
                if (value == _script) return;
                _script = value;
                OnPropertyChanged();
            }
        }

        public string TargetEnvironment
        {
            get { return _targetEnvironment; }
            set
            {
                if (value == _targetEnvironment) return;
                _targetEnvironment = value;
                OnPropertyChanged();
            }
        }

        public Guid AssociatedAgentInstanceGuid
        {
            get { return _associatedAgentInstanceGuid; }
            set
            {
                if (value.Equals(_associatedAgentInstanceGuid)) return;
                _associatedAgentInstanceGuid = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }
    }

    public class SensorComparer : IEqualityComparer<Sensor>
    {
        public bool Equals(Sensor x, Sensor y)
        {
            return x.Name == y.Name && x.TargetEnvironment == y.TargetEnvironment && x.AssociatedAgentInstanceGuid == y.AssociatedAgentInstanceGuid;
        }

        public int GetHashCode(Sensor obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Name.GetHashCode() ^ obj.TargetEnvironment.GetHashCode() ^ obj.AssociatedAgentInstanceGuid.GetHashCode();
        }
    }
}