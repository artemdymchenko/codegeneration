﻿using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class AgentRole : BaseEntity
    {
        private string _role;

        public AgentRole(string role)
        {
            _role = role;
        }

        //For serialization
        public AgentRole()
        {
        }

        public string Role
        {
            get { return _role; }
            set
            {
                if (value == _role) return;
                _role = value;
                OnPropertyChanged();
            }
        }
    }

    public class AgentComparer : IEqualityComparer<AgentRole>
    {
        public bool Equals(AgentRole x, AgentRole y)
        {
            return x.Role == y.Role;
        }

        public int GetHashCode(AgentRole obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Role.GetHashCode();
        }
    }
}