﻿using System;
using System.Collections.Generic;

namespace Drawer.Data.Entities
{
    public class AgentInstance : BaseEntity
    {
        public AgentInstance(Guid agentRoleGuid, string name)
        {
            AgentRoleGuid = agentRoleGuid;
            Name = name;
        }

        public Guid AgentRoleGuid { get; set; }
        public string Name { get; set; }
    }

    public class AgentInstanceComparer : IEqualityComparer<AgentInstance>
    {
        public bool Equals(AgentInstance x, AgentInstance y)
        {
            return x.Name == y.Name;
        }

        public int GetHashCode(AgentInstance obj)
        {
            if (ReferenceEquals(obj, null))
                return 0;

            return obj.Name.GetHashCode();
        }
    }
}