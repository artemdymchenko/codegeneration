﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Drawer.Annotations;
using Drawer.Data.Entities;
using Environment = Drawer.Data.Entities.Environment;

namespace Drawer.Data
{
    public class DataHolder : INotifyPropertyChanged
    {
        public bool BlockUpdate { get; set; }
        public Environment Environment { get; set; }
        public ObservableCollection<BeliefsTemplate> BeliefsTemplates { get; set; }
        public BlackBoard BlackBoard { get; set; }
        public ObservableCollection<StateTemplate> StateTemplates { get; set; }
        public ObservableCollection<Goal> Goals { get; set; }
        public ObservableCollection<Protocol> Protocols { get; set; }
        public ObservableCollection<Sensor> Sensors { get; set; }
        public ObservableCollection<AgentRole> AgentRoles { get; set; }
        public ObservableCollection<AgentInstance> AgentInstances { get; set; }
        public ObservableCollection<Plan> Plans { get; set; }

        public DataHolder()
        {
            BlockUpdate = false;
            Environment = new Environment();
            BeliefsTemplates = new ObservableCollection<BeliefsTemplate>();
            BlackBoard = new BlackBoard();
            StateTemplates = new ObservableCollection<StateTemplate>();
            Goals = new ObservableCollection<Goal>();
            Protocols = new ObservableCollection<Protocol>();
            Sensors = new ObservableCollection<Sensor>();
            AgentRoles = new ObservableCollection<AgentRole>();
            AgentInstances = new ObservableCollection<AgentInstance>();
            Plans = new ObservableCollection<Plan>();

            AttachNotifiers();
        }

        private void AttachNotifiers()
        {
            Environment.PropertyChanged += DataChanged;
            BeliefsTemplates.CollectionChanged += ListDataChanged;
            BlackBoard.PropertyChanged += DataChanged;
            StateTemplates.CollectionChanged += ListDataChanged;
            Goals.CollectionChanged += ListDataChanged;
            Protocols.CollectionChanged += ListDataChanged;
            Sensors.CollectionChanged += ListDataChanged;
            AgentRoles.CollectionChanged += ListDataChanged;
            AgentInstances.CollectionChanged += ListDataChanged;
            Plans.CollectionChanged += ListDataChanged;
        }

        private void DettachNotifiers()
        {
            Environment.PropertyChanged -= DataChanged;
            BeliefsTemplates.CollectionChanged -= ListDataChanged;
            BlackBoard.PropertyChanged -= DataChanged;
            StateTemplates.CollectionChanged -= ListDataChanged;
            Goals.CollectionChanged -= ListDataChanged;
            Protocols.CollectionChanged -= ListDataChanged;
            Sensors.CollectionChanged -= ListDataChanged;
            AgentRoles.CollectionChanged -= ListDataChanged;
            AgentInstances.CollectionChanged -= ListDataChanged;
            Plans.CollectionChanged -= ListDataChanged;
        }

        public void UpdateData(DataHolder newData)
        {
            Environment = newData.Environment;
            BeliefsTemplates = newData.BeliefsTemplates;
            BlackBoard = newData.BlackBoard;
            StateTemplates = newData.StateTemplates;
            Goals = newData.Goals;
            Protocols = newData.Protocols;
            Sensors = newData.Sensors;
            AgentRoles = newData.AgentRoles;
            AgentInstances = newData.AgentInstances;
            Plans = newData.Plans;

            AttachNotifiers();
            OnPropertyChanged();
        }

        public void ClearData()
        {
            Environment.Clear();
            BeliefsTemplates.Clear();
            BlackBoard.Clear();
            StateTemplates.Clear();
            Goals.Clear();
            Protocols.Clear();
            Sensors.Clear();
            AgentRoles.Clear();
            AgentInstances.Clear();
            Plans.Clear();

            //DettachNotifiers();
            OnPropertyChanged();
        }

        public bool Equals(DataHolder anotherInstance)
        {
            var environmentEquality = Environment.Equals(anotherInstance.Environment);
            var beliefsEquality = BeliefsTemplates.OrderBy(t => t.Name).SequenceEqual(anotherInstance.BeliefsTemplates.OrderBy(t => t.Name), new BeliefsTemplateComparer());
            var statesEquality = StateTemplates.OrderBy(t => t.Name).SequenceEqual(anotherInstance.StateTemplates.OrderBy(t => t.Name), new StateTemplateComparer());
            var plansEquality = Plans.OrderBy(t => t.Name).SequenceEqual(anotherInstance.Plans.OrderBy(t => t.Name), new PlanComparer());
            var goalsEquality = Goals.OrderBy(t => t.Name).SequenceEqual(anotherInstance.Goals.OrderBy(t => t.Name), new GoalComparer());
            var protocolsEquality = Protocols.OrderBy(t => t.Name).SequenceEqual(anotherInstance.Protocols.OrderBy(t => t.Name), new ProtocolComparer());
            var sensorsEquality = Sensors.OrderBy(t => t.Name).SequenceEqual(anotherInstance.Sensors.OrderBy(t => t.Name), new SensorComparer());
            var agentEquality = AgentRoles.OrderBy(t => t.Role).SequenceEqual(anotherInstance.AgentRoles.OrderBy(t => t.Role), new AgentComparer());
            var agentInstanceEquality = AgentInstances.OrderBy(t => t.Name).SequenceEqual(anotherInstance.AgentInstances.OrderBy(t => t.Name), new AgentInstanceComparer());

            return environmentEquality && beliefsEquality && statesEquality && plansEquality
                && goalsEquality && protocolsEquality && sensorsEquality && agentEquality;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                if (!BlockUpdate) 
                    handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ListDataChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (BaseEntity newItem in e.NewItems)
                {
                    newItem.PropertyChanged += DataChanged;
                }
            }
 
            OnPropertyChanged();
        }

        private void DataChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged();
        }
    }
}