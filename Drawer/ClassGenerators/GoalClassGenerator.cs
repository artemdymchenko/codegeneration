﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;
using CodeGenerator.XMLEntities;

namespace CodeGenerator.ClassGenerators
{
    public class GoalClassGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "GoalTemplate.tmpl";
        private static readonly string TemplateInterfaceFile = Paths.TemplatesPath + "IGoalTemplate.tmpl";
        private static readonly string TemplateForGetPlans = Paths.TemplatesPath + "GetPlansScript.tmpl";
        public static readonly List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, List<XMLPlan> plans, List<XMLGoal> goals)
        {
            var sources = new List<string>();
            var interfaceSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(interfaceSb, TemplateInterfaceFile);
            interfaceSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
            sources.Add(interfaceSb.ToString());
            ClassNames.Add("IGoal");

            foreach (var xmlgoal in goals)
            {
                var goalSb = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(goalSb, TemplateFile);
                goalSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                goalSb.Replace(BasicAnnotations.ClassName, xmlgoal.Name);
                goalSb.Replace("[TargetState]", xmlgoal.TargetState);

                var isAchievedScriptBs = new StringBuilder();
                isAchievedScriptBs.Append("//************");

                var plansForTheGoal = plans.Where(xmlPlan => xmlPlan.TargetGoal == xmlgoal.Name).ToList();

                var plansSb = new StringBuilder();
                foreach (var xmlPlan in plansForTheGoal)
                {
                    IoHelper.GetFileTextToStringBuilder(plansSb, TemplateForGetPlans);
                    plansSb.Replace("[PlanName]", xmlPlan.PlanName);
                }

                goalSb.Replace("[Plans]", plansSb.ToString());

                ClassNames.Add(xmlgoal.Name);
                sources.Add(goalSb.ToString());
            }
            return sources;
        }  
    }
}