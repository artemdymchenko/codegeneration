﻿using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public class BaseSensorGenerator
    {
        private static readonly string TemplatePath = Paths.TemplatesPath + "BaseSensorTemplate.tmlp";

        public static readonly string ClassName = "BaseSensor.cs";

        public static string GenerateSource(string namespaceName)
        {
            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, TemplatePath);

            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);

            return mainSb.ToString();
        }     
    }
}