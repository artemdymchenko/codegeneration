﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;
using CodeGenerator.XMLEntities;

namespace CodeGenerator.ClassGenerators
{
    public class PlanClassGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "PlanTemplate.tmpl";
        private static readonly string TemplateInterfaceFile = Paths.TemplatesPath + "IPlanTemplate.tmpl";
        public static readonly List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, XmlParser xmlParser)
        {
            var sources = new List<string>();
            var mainSb = new StringBuilder();
            var plans = xmlParser.Plans;
            var goals = xmlParser.Goals;
            IoHelper.GetFileTextToStringBuilder(mainSb, TemplateInterfaceFile);
            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
            sources.Add(mainSb.ToString());
            ClassNames.Add("IPlan");

            foreach (var xmlPlan in plans)
            {
                var funcTypesSb = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(funcTypesSb, TemplateFile);
                funcTypesSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                funcTypesSb.Replace(BasicAnnotations.ClassName, xmlPlan.PlanName);
                funcTypesSb.Replace("[GoalName]", xmlPlan.TargetGoal);

                var g = goals.Find(goal => goal.Name == xmlPlan.TargetGoal);
                funcTypesSb.Replace("[StateName]", g.TargetState);

                var executePlan = new StringBuilder();
                var source = xmlPlan.ExecutePlanScriptDestination;
                IoHelper.GetFileTextToStringBuilder(executePlan, xmlParser.SourceFolder + source);
                if (executePlan.ToString() == "")
                {
                    executePlan.Append("//************");
                }  
                var getUtility = new StringBuilder();
                getUtility.Append("return 10;");

                if (xmlPlan.ExecutePlanScriptDestination != "")
                    IoHelper.GetFileTextToStringBuilder(executePlan, xmlPlan.ExecutePlanScriptDestination);

                if (xmlPlan.UtilityScriptDestination != "")
                    IoHelper.GetFileTextToStringBuilder(getUtility, xmlPlan.UtilityScriptDestination);

                funcTypesSb.Replace("[ExecutePlan]", executePlan.ToString());
                funcTypesSb.Replace("[GetUtility]", getUtility.ToString());

                ClassNames.Add(xmlPlan.PlanName);
                sources.Add(funcTypesSb.ToString());
            }

            return sources;
        }  
    }
}