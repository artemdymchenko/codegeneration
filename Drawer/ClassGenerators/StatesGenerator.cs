﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;
using CodeGenerator.XMLEntities;

namespace CodeGenerator.ClassGenerators
{
    public class StatesGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "StatesTemplate.tmpl";
        public static readonly List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, List<XMLState> states)
        {
            var sources = new List<string>();
            foreach (var xmlState in states)
            {
                var stateSB = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(stateSB, TemplateFile);
                stateSB.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                stateSB.Replace(BasicAnnotations.ClassName, xmlState.Name);
                ClassNames.Add(xmlState.Name);

                var attributesSb = new StringBuilder();
                var attributesInitSb = new StringBuilder();
                foreach (var xmlAttribute in xmlState.Attributes)
                {
                    var type = xmlAttribute.Type;
                    var name = xmlAttribute.Name;

                    if (xmlAttribute.Type.Contains("!"))
                    {
                        var v = xmlAttribute.Type.Split('!');
                        type = v[0] + "<" + v[1] + ">";
                    }
                    string attr;
                    string attrInit;
                    switch (type)
                    {
                        case "string":
                            attrInit = name + @" = """";" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        case "int":
                            attrInit = name + @" = 0;" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        case "bool":
                            attrInit = name + @" = false;" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        default:
                            attrInit = name + @" = new " + type + "();\n\t\t\t";
                            attr = "public " + type + " " + name + " { get; set; }\n\t\t";
                            break;
                    }
                    //attrInit = name + " = new " + type + "();\n\t\t\t";
                    attributesSb.Append(attr);
                    attributesInitSb.Append(attrInit);
                }
                stateSB.Replace("[attrs]", attributesSb.ToString());
                
                var protocolsSb = new StringBuilder();
                foreach (var protocolName in xmlState.Protocols)
                {
                    protocolsSb.Append("public " + protocolName + " " + protocolName + " { get; private set; }\n\t\t");
                    attributesInitSb.Append(protocolName + " = new " + protocolName + "();\n\t\t");
                }

                stateSB.Replace("[protocols]", protocolsSb.ToString());
                stateSB.Replace("[attrsInit]", attributesInitSb.ToString());
                sources.Add(stateSB.ToString());
            }
            return sources;
        } 
 
    }
}