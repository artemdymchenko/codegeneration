﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using CodeGenerator.XMLEntities;

namespace CodeGenerator
{
    public class XmlParser
    {
        public string   DestinationFolder { get; private set; }
        public string NamespaceName { get; private set; }
        public List<XMLAttribute> EnvironmentAttributes { get; private set; }
        public List<XMLProtocol> Protocols { get; private set; }
        public List<XMLState> States { get; private set; }
        public List<XMLPlan> Plans { get; private set; }
        public List<XMLGoal> Goals { get; private set; }
        public List<XMLNamedAttributeList> Beliefs { get; private set; }
        public List<XMLAgentRole> AgentRoles { get; private set; }
        public List<XMLAgentInstance> AgentInstances { get; private set; }

        public string SourceFolder { get; set; }

        public XmlParser(string inputXmlPath)
        {
            SourceFolder = inputXmlPath.Substring(0, inputXmlPath.LastIndexOf(@"\", StringComparison.Ordinal));
            EnvironmentAttributes = new List<XMLAttribute>();
            Protocols = new List<XMLProtocol>();
            States = new List<XMLState>();
            Plans = new List<XMLPlan>();
            Goals = new List<XMLGoal>();
            Beliefs = new List<XMLNamedAttributeList>();
            AgentRoles = new List<XMLAgentRole>();
            AgentInstances = new List<XMLAgentInstance>();

            var input = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(input, inputXmlPath);

            using (var reader = XmlReader.Create(new StringReader(input.ToString())))
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement)
                        continue;

                    switch (reader.Name)
                    {
                        case "Architecture":
                            DestinationFolder = reader.GetAttribute("destination-folder");
                            NamespaceName = reader.GetAttribute("namespace-name");
                            break;
                        case "Environment":
                            ReadAttributeTags(reader, EnvironmentAttributes);
                            break;
                        case "Protocols":
                            ReadProtocolsTag(reader);
                            break;
                        case "States":
                            ReadStatesTag(reader);
                            break;
                        case "Plans":
                            ReadPlansTag(reader);
                            break;
                        case "Goals":
                            ReadGoalsTag(reader);
                            break;
                        case "Beliefs":
                            ReadBeliefsTag(reader);
                            break;
                        case "Agents":
                            ReadAgentsTag(reader);
                            break;
                    }
                }
        }

        private void ReadAgentsTag(XmlReader reader)
        {
            reader.ReadToDescendant("AgentRoles");
            reader.ReadToDescendant("AgentRole");
            do
            {
                var name = reader.GetAttribute("name");
                var state = reader.GetAttribute("TargetState");
                var belief = reader.GetAttribute("TargetBelief");
                var protocols = new List<string>();
                reader.ReadToDescendant("ProtocolConnections");
                reader.ReadToDescendant("ProtocolConnection");
                do
                {
                    protocols.Add(reader.GetAttribute("name"));
                } while (reader.ReadToNextSibling("ProtocolConnection"));
                AgentRoles.Add(new XMLAgentRole(name, state, belief, protocols));
                reader.ReadToNextSibling("ProtocolConnections");
            } while (reader.ReadToNextSibling("AgentRole"));

            reader.ReadToNextSibling("AgentInstances");
            reader.ReadToDescendant("AgentInstance");
            do
            {
                var name = reader.GetAttribute("name");
                var role = reader.GetAttribute("role");
                reader.ReadToDescendant("Sensors");
                reader.ReadToDescendant("Sensor");
                var sensors = new List<XMLSensor>();
                do
                {
                    sensors.Add(new XMLSensor(reader.GetAttribute("path"), reader.GetAttribute("attributeName"),
                        reader.GetAttribute("name")));
                } while (reader.ReadToNextSibling("Sensor"));
                reader.ReadToNextSibling("Sensors");
                AgentInstances.Add(new XMLAgentInstance(name, role, sensors));
            } while (reader.ReadToNextSibling("AgentInstance"));
        }

        private void ReadBeliefsTag(XmlReader reader)
        {
            reader.ReadToDescendant("Belief");
            do
            {
                var belief = new XMLNamedAttributeList(reader.GetAttribute("name"));
                if (!reader.IsEmptyElement)
                {
                    ReadAttributeTags(reader, belief.Attributes);
                }
                Beliefs.Add(belief);
            } while (reader.ReadToNextSibling("Belief"));
        }

        private void ReadGoalsTag(XmlReader reader)
        {
            reader.ReadToDescendant("Goal");
            do
            {
                var name = reader.GetAttribute("name");
                var targetState = reader.GetAttribute("TargetState");
                Goals.Add(new XMLGoal(name, targetState));
            } while (reader.ReadToNextSibling("Goal"));
        }

        private void ReadPlansTag(XmlReader reader)
        {
            reader.ReadToDescendant("Plan");
            do
            {
                var name = reader.GetAttribute("name");
                var targetGoal = reader.GetAttribute("TargetGoal");;
                var utilityScriptPath = reader.GetAttribute("Utility");
                var executePlanScript = reader.GetAttribute("ExecutePlan");
                Plans.Add(new XMLPlan(name, targetGoal, executePlanScript, utilityScriptPath));
            } while (reader.ReadToNextSibling("Plan"));
        }

        private void ReadStatesTag(XmlReader reader)
        {
            reader.ReadToDescendant("State");
            do
            {
                var state = new XMLState(reader.GetAttribute("name"));
                bool hasProtocols = false;
                if (!reader.IsEmptyElement)
                {
                    reader.ReadToDescendant("Attributes");
                    ReadAttributeTags(reader, state.Attributes);

                    reader.ReadToNextSibling("Protocols");
                    reader.ReadToDescendant("Protocol");
                    do
                    {
                        hasProtocols = true;
                        state.Protocols.Add(reader.GetAttribute("name"));
                    } while (reader.ReadToNextSibling("Protocol"));
                }
                if (hasProtocols)
                    reader.ReadToNextSibling("Protocols");
                States.Add(state);
            } while (reader.ReadToNextSibling("State"));
        }

        private void ReadProtocolsTag(XmlReader reader)
        {
            reader.ReadToDescendant("Protocol");
            do
            {
                var protocol = new XMLProtocol(reader.GetAttribute("name"));
                reader.ReadToDescendant("Func");
                do
                {
                    var source = reader.GetAttribute("source");
                    reader.ReadToDescendant("Name");
                    var name = reader.ReadInnerXml();
                    var parameterTypes = new List<string>();
                    while (reader.ReadToNextSibling("Parameter"))
                        parameterTypes.Add(reader.ReadInnerXml());

                    protocol.Funcs.Add(new XMLProtocolFunc(name, source, parameterTypes));
                } while (reader.ReadToNextSibling("Func"));
                Protocols.Add(protocol);
            } while (reader.ReadToNextSibling("Protocol"));
        }

        private static void ReadAttributeTags(XmlReader reader, ICollection<XMLAttribute> listToFill)
        {
            reader.ReadToDescendant("Attribute");
            do
            {
                var name = reader.GetAttribute("name");
                var type = reader.GetAttribute("type");
                listToFill.Add(new XMLAttribute(name, type));
            } while (reader.ReadToNextSibling("Attribute"));
        }
    }
}