﻿using Drawer.Data;

namespace Drawer.XMLGenerator
{
    public interface IXMLGenerator
    {
        void GenerateXml(DataHolder data);
    }
}