﻿namespace Drawer.XMLGenerator
{
    public class BasicAnnotations
    {
         public static readonly string DestinationFolder = "[DESTINATION_FOLDER]";
         public static readonly string NamespaceName = "[NAMESPACE_NAME]";
         public static readonly string Attributes = "[ATTRIBUTES]";
         public static readonly string AttributeName = "[ATTRIBUTE_NAME]";
         public static readonly string AttributeType = "[ATTRIBUTE_TYPE]";
    }
}