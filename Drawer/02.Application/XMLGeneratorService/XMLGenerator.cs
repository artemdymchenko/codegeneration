﻿using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

using Drawer.Data;

namespace Drawer.XMLGenerator
{
    public class XmlGenerator : IXMLGenerator
    {
        public static string FirstLineTamplate = @"..\..\..\XMLTemplates\FirstLineTemplate.tmlp";
        public static string EnvironmentTemplate = @"..\..\..\XMLTemplates\Environment.tmlp";
        public static string EnvironmentAttributeTemplate = @"..\..\..\XMLTemplates\EnvironmentAttribute.tmlp";
        public static string DestinationFolder = @"..\..\..\GeneratedXML\";
        public static string NamespaceName = "TestNamespace";

        public void GenerateXml(DataHolder data)
        {
            var firstLine = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(firstLine, FirstLineTamplate);

            var mainSb = new StringBuilder();
            mainSb.Append("<?xml version='1.0' encoding='utf-8' ?>\n");
            mainSb.Append(
                "<Architecture destination-folder = 'GeneratedCode/' namespace-name = 'GeneratedArchitecture'>\n");

            mainSb.Append(GetEnvironmentXml(data));
            mainSb.Append(GetProtoolsXml(data));
            mainSb.Append(GetStatesXml(data));
            mainSb.Append(GetGoalsXml(data));
            mainSb.Append(GetPlansXml(data));
            mainSb.Append(GetBeliefsXml(data));
            mainSb.Append(GetAgentsXml(data));

            mainSb.Append("</Architecture>");

            //write to xml file
            //var file = new StreamWriter(DestinationFolder + "input.xml");
            var file = new StreamWriter(Directory.GetCurrentDirectory() + "\\" + "input.xml");
            file.WriteLine(mainSb.ToString());
            file.Close();
        }

        //TODO: finish scripting
        private string GetAgentsXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("\t<Agents>\n");
            sb.Append("\t\t<AgentRoles>\n");
            //AgentRole roles

            foreach (var agent in data.AgentRoles)
            {
                var targetState =
                    data.StateTemplates.FirstOrDefault(state => state.AssociatedAgentRoleGuid == agent.Guid);

                var targetBelief =
                    data.BeliefsTemplates.FirstOrDefault(belief => belief.AssociatedAgentRoleGuid == agent.Guid);

                var protocolsSb = new StringBuilder();

                var protocols = data.Protocols.Where(prot => prot.Server == agent.Guid);
                if (protocols.Count() > 0)
                {
                    protocolsSb.Append("\t\t\t\t<ProtocolConnections>\n");
                    foreach (var protocol in protocols)
                    {
                        protocolsSb.Append("\t\t\t\t\t<ProtocolConnection name = '" + protocol.Name + "'/>\n");
                    }
                    protocolsSb.Append("\t\t\t\t</ProtocolConnections>\n");
                }
                sb.Append(
                "\t\t\t<AgentRole name = '" + agent.Role + "' TargetState = '" 
                + (targetState == null ? "" : targetState.Name)
                + "' TargetBelief = '" + (targetBelief == null ? "" : targetBelief.Name) + "' >\n" +
                //"\t\t\t\t<ProtocolConnections>\n" +
                //"\t\t\t\t\t<ProtocolConnection name = '" + "'/>\n" +
                //"\t\t\t\t</ProtocolConnections>\n" +
                protocolsSb + 
                "\t\t\t</AgentRole>\n"
                );
            }

            sb.Append("\t\t</AgentRoles>\n");
            sb.Append("\t\t<AgentInstances>\n");
            //AgentRole instances
            foreach (var agent in data.AgentInstances)
            {
                //find role name by guid 
                var role = data.AgentRoles.FirstOrDefault(r => r.Guid == agent.AgentRoleGuid);
                if(role == null)
                    continue;

                sb.Append("\t\t\t<AgentInstance name = '" + agent.Name + "' role = '" + role.Role + "'>\n");
                sb.Append("\t\t\t\t<Sensors>\n");
                foreach (var sensor in data.Sensors)
                {
                    if (agent.Guid == sensor.AssociatedAgentInstanceGuid)
                        sb.Append("\t\t\t\t\t<Sensor name = '" + sensor.Name + 
                            "' attributeName = '" + sensor.TargetEnvironment + "' path = '" + sensor.Script + "'/>\n");
                }
                sb.Append("\t\t\t\t</Sensors>\n");
                sb.Append("\t\t\t</AgentInstance>\n");
            }
            sb.Append("\t\t</AgentInstances>\n");
            sb.Append("\t</Agents>\n");
            return sb.ToString();
        }

        private string GetBeliefsXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("\t<Beliefs>\n");
            foreach (var belief in data.BeliefsTemplates)
            {
                if (belief.Attribs.Count > 0)
                {
                    sb.Append("\t\t<Belief name = '" + belief.Name + "'>\n");
                    foreach (var attrib in belief.Attribs)
                    {
                        sb.Append("\t\t\t<Attribute name = '" + attrib.Value + "' type = '" + attrib.Key + "'/>\n");
                    }
                    sb.Append("\t\t</Belief>\n");
                }
                else
                    sb.Append("\t\t<Belief name = '" + belief.Name + "'/>\n");
            }
            sb.Append("\t</Beliefs>\n");
            return sb.ToString();
        }

        //TODO: finish scripting
        private string GetPlansXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("\t<Plans>\n");
            foreach (var plan in data.Plans)
            {
                sb.Append("\t\t<Plan name = '"+ plan.Name + "' TargetGoal = '" + plan.Goal.Name + "' " +
                          "Utility = '" + "' ExecutePlan = '" + plan.ExecuteScriptPath + "'/>\n");
            }
            sb.Append("\t</Plans>\n");
            return sb.ToString();
        }

        private string GetGoalsXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("\t<Goals>\n");
            foreach (var goal in data.Goals)
            {
                sb.Append("\t\t<Goal name = '" + goal.Name + "' TargetState = '" + goal.AssociatedStateName + "'/>\n");
            }
            sb.Append("\t</Goals>\n");
            return sb.ToString();
        }

        //TODO: Finish it
        private string GetStatesXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("\t<States>\n");
            foreach (var stateTemplate in data.StateTemplates)
            {
                if (stateTemplate.Attribs.Count > 0)
                {
                    sb.Append("\t\t<State name = '" + stateTemplate.Name + "'>\n");
                    if (stateTemplate.Attribs.Count > 0)
                    {
                        sb.Append("\t\t\t<Attributes>\n");
                        foreach (var attrib in stateTemplate.Attribs)
                        {
                            sb.Append("\t\t\t\t<Attribute name = '" + attrib.Value + "' type = '" + attrib.Key + "'/>\n");
                        }
                        sb.Append("\t\t\t</Attributes>\n");
                    }
                }
                else
                {
                    sb.Append("\t\t<State name = '" + stateTemplate.Name + "'/>\n");
                }

                var targetRole =
                    data.AgentRoles.FirstOrDefault(role => role.Guid == stateTemplate.AssociatedAgentRoleGuid);

                int targetClientProtocolsCount = 0;
                if (targetRole != null)
                {
                    var targetClientProtocols = data.Protocols.Where(prot => prot.Client == targetRole.Guid);
                    if (targetClientProtocols.Count() > 0)
                    {
                        targetClientProtocolsCount = targetClientProtocols.Count();
                        sb.Append("\t\t\t<Protocols>\n");
                        foreach (var targetClientProtocol in targetClientProtocols)
                        {
                            sb.Append("\t\t\t\t<Protocol name = '" + targetClientProtocol.Name + "'/>\n");
                        }
                        sb.Append("\t\t\t</Protocols>\n");
                    }
                }

                if (stateTemplate.Attribs.Count > 0 || targetClientProtocolsCount > 0)
                sb.Append("\t\t</State>\n");
            }
            sb.Append("\t</States>\n");
            return sb.ToString();
        }

        private string GetProtoolsXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("<Protocols>\n");
            foreach (var protocol in data.Protocols)
            {
                sb.Append("\t<Protocol name = '" + protocol.Name + "'>\n");
                foreach (var protocolMethod in protocol.ProtocolMethods)
                {
                    sb.Append("\t\t<Func source = '" + protocolMethod.Source + "'>\n");
                    sb.Append("\t\t\t<Name>" + protocolMethod.MethodName + "</Name>\n");
                    foreach (var type in protocolMethod.Types)
                    {
                        sb.Append("\t\t\t<Parameter>" + type + "</Parameter>\n");
                    }
                    sb.Append("\t\t</Func>\n");
                }
                sb.Append("\t</Protocol>\n");
            }
            sb.Append("</Protocols>\n");
            return sb.ToString();
        }

        private string GetEnvironmentXml(DataHolder data)
        {
            var sb = new StringBuilder();
            sb.Append("<Environment>\n");
            foreach (var attrib in data.Environment.Attribs)
            {
                sb.Append("\t<Attribute name = '" + attrib.Value + "' type = '" + attrib.Key + "'/>\n");
            }
            sb.Append("</Environment>\n");
            return sb.ToString();
        }
    }
}