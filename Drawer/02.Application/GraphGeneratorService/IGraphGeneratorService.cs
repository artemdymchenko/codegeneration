﻿

using Drawer.CustomTypes.VertexTypes;
using Drawer.Data;
using QuickGraph;

namespace Drawer.Application
{
    public interface IGraphGeneratorService
    {
        BidirectionalGraph<object, IEdge<object>> GenerateGraphToVisualize(DataHolder data);
        }
}