﻿using System;
using System.Linq;
using Drawer.CustomTypes.VertexTypes;

using Drawer.Data;
using QuickGraph;

namespace Drawer.Application
{
    public class GraphGeneratorService : IGraphGeneratorService
    {
        public BidirectionalGraph<object, IEdge<object>> GenerateGraphToVisualize(DataHolder data)
        {
            var graph = new BidirectionalGraph<object, IEdge<object>>();

            AddEnvironmentAttributes(data, graph);
            AddAgentRoles(data, graph);
            AddBeliefs(data, graph);
            AddSensors(data, graph);
            AddGoals(data, graph);
            AddPlans(data, graph);
            AddProtocols(data, graph);
            AddStates(data, graph);
            AddAgentinstances(data, graph);

            AddEdges(graph);

            if (graph.VertexCount == 0)
            {
                graph.AddVertex(new DummyVertex());
            }
            return graph;
        }

        #region Private Methods
        private void AddEnvironmentAttributes(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var env = data.Environment;
            foreach (var item in env.Attribs)
            {
                BaseVertex newVertex = new EnvironmentAttributeVertex
                {
                    EnvironemntAttributeName = item.Key,
                    EnvironemntAttributeType = item.Value
                };
                graph.AddVertex(newVertex);
            }
        }

        private void AddAgentRoles(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.AgentRoles;
            foreach (var item in arr)
            {
                BaseVertex newVertex = new AgentRoleVertex(item.Role) {AgentRoleGuid = item.Guid};
                graph.AddVertex(newVertex);
            }
        }

        private void AddAgentinstances(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.AgentInstances;
            foreach (var item in arr)
            {
                BaseVertex newVertex = new AgentInstVertex(item.Name)
                {
                    AgentInstanceGuid = item.Guid,
                    AgentInstanceRoleGuid = item.AgentRoleGuid
                };
                graph.AddVertex(newVertex);
            }
        }

        private void AddBeliefs(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.BeliefsTemplates;
            foreach (var item in arr)
            {
                BaseVertex newVertex = new BaliefsTemplateVertex(item.Name)
                {
                    AssociatedWithBeliefAgentGuid = item.AssociatedAgentRoleGuid
                };

                foreach (var attrib in item.Attribs)
                {
                    newVertex.BeliefsAttribs.Add(new TypeName
                    {
                        Type = attrib.Value, 
                        Name = attrib.Key,
                    });
                }
                graph.AddVertex(newVertex);
            }
        }

        private void AddSensors(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.Sensors;
            foreach (var item in arr)
            {
                var newVertex = new SensorVertex(item.Name)
                {
                    AssociatedAgentGuidToThisSensor = item.AssociatedAgentInstanceGuid,
                    AssociatedEnvironmentAttributeNameToThisSensor = item.TargetEnvironment,
                };

                graph.AddVertex(newVertex);
            }
        }

        private void AddGoals(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.Goals;
            foreach (var item in arr)
            {
                BaseVertex newVertex = new GoalVertex(item.Name)
                {
                    AssociatedStateName = item.AssociatedStateName
                };

                graph.AddVertex(newVertex);
            }
        }

        private void AddPlans(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.Plans;
            foreach (var item in arr)
            {
                var newVertex = new PlanVertex(item.Name);

                if (item.Goal != null)
                    newVertex.AssociatedGoalNameToThisPlan = item.Goal.Name;

                graph.AddVertex(newVertex);
            }
        }

        private void AddProtocols(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.Protocols;
            foreach (var item in arr)
            {
                var newVertex = new ProtocolVertex(item.Name)
                {
                    AssociatedClientAgentGuidToThisProtocol = item.Client,
                    AssociatedServerAgentGuidToThisProtocol = item.Server
                };

                graph.AddVertex(newVertex);
            }
        }

        private void AddStates(DataHolder data, BidirectionalGraph<object, IEdge<object>> graph)
        {
            var arr = data.StateTemplates;
            foreach (var item in arr)
            {
                BaseVertex newVertex = new StateVertex(item.Name)
                {
                    AssociatedWithStateAgentGuid = item.AssociatedAgentRoleGuid
                };

                foreach (var attrib in item.Attribs)
                {
                    newVertex.StateAttribs.Add(new TypeName
                    {
                        Type = attrib.Value,
                        Name = attrib.Key,
                    });
                }
                graph.AddVertex(newVertex);
            }
        }

        private void AddEdges(BidirectionalGraph<object, IEdge<object>> graph)
        {
            foreach (var vertex in graph.Vertices)
            {
                CheckParameterConnections(graph, b => b.AssociatedWithBeliefAgentGuid, b => b.AgentRoleGuid, ((BaseVertex)vertex));
                CheckParameterConnections(graph, b => b.AssociatedWithStateAgentGuid, b => b.AgentRoleGuid, ((BaseVertex)vertex));
                CheckParameterConnections(graph, b => b.AgentInstanceRoleGuid, b => b.AgentRoleGuid, ((BaseVertex)vertex), true);
                CheckParameterConnections(graph, b => b.AgentInstanceRoleGuid, b => b.AgentRoleGuid, ((BaseVertex)vertex), true);

                CheckParameterConnections(graph, b => b.AssociatedEnvironmentAttributeNameToThisSensor, b => b.EnvironemntAttributeName,
                    ((BaseVertex)vertex));

                CheckParameterConnections(graph, b => b.GoalName, b => b.AssociatedGoalNameToThisPlan, ((BaseVertex)vertex), true);
                CheckParameterConnections(graph, b =>
                {
                    var agent = (BaseVertex)graph.Vertices.FirstOrDefault(vert =>
                    {
                        if (((BaseVertex)vert).AgentInstanceGuid == Guid.Empty)
                            return false;
                        var res = (((BaseVertex)vert).AgentInstanceGuid == b.AssociatedAgentGuidToThisSensor);
                        return res;
                    });

                    if (agent != null)
                        return agent.AgentInstanceName;
                    return "";
                }, b => b.AgentInstanceName, ((BaseVertex)vertex), true);

                CheckParameterConnections(graph, b => b.StateName, b => b.AssociatedStateName, ((BaseVertex)vertex), true);
                CheckParameterConnections(graph, b => b.AssociatedClientAgentGuidToThisProtocol, b => b.AgentRoleGuid,
                    ((BaseVertex)vertex), true);

                CheckParameterConnections(graph, b => b.AssociatedServerAgentGuidToThisProtocol, b => b.AgentRoleGuid,
                    ((BaseVertex)vertex));
            }
        }

        private static void CheckParameterConnections(BidirectionalGraph<object, IEdge<object>> graph, Func<BaseVertex, Guid> parameterToCheck,
            Func<BaseVertex, Guid> correspondingGuidParameter, BaseVertex baseVertex, bool reverseConnection = false)
        {
            if (parameterToCheck(baseVertex) != Guid.Empty)
            {
                var searchedInstances =
                        graph.Vertices.Where(
                            vert => (correspondingGuidParameter((BaseVertex)vert)) == parameterToCheck(baseVertex));

                foreach (var searchedInstance in searchedInstances)
                {
                    graph.AddEdge(reverseConnection
                        ? new Edge<object>(searchedInstance, baseVertex)
                        : new Edge<object>(baseVertex, searchedInstance));
                }
            }
        }

        private static void CheckParameterConnections(BidirectionalGraph<object, IEdge<object>> graph, Func<BaseVertex, string> parameterToCheck,
            Func<BaseVertex, string> correspondingParameter, BaseVertex baseVertex, bool reverseConnection = false)
        {
            if (parameterToCheck(baseVertex) != null)
            {
                var searchedInstances =
                        graph.Vertices.Where(
                            vert => (correspondingParameter((BaseVertex)vert)) == parameterToCheck(baseVertex));

                foreach (var searchedInstance in searchedInstances)
                {
                    graph.AddEdge(reverseConnection
                        ? new Edge<object>(searchedInstance, baseVertex)
                        : new Edge<object>(baseVertex, searchedInstance));
                }
            }
        }
        #endregion
    }
}