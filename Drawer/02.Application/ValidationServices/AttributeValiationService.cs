﻿using System.Collections.Generic;
using System.Windows.Documents;
using Drawer.Application.AttributeService;

namespace Drawer
{
    public class AttributeValidationValidationService : IAttributeValidationService
    {
        public AttributeValidationValidationService()
        {
            AcceptableTypes = new List<string>
            {
                "int",
                "double",
                "float",
                "bool",
                "string",
                "List!string",
                "List!int"
            };
        }

        private List<string> AcceptableTypes { get; set; }

        public bool ValidateAttribute(string attr)
        {
            return AcceptableTypes.Contains(attr);
        }
    }
}