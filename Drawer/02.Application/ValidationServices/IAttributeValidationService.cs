﻿namespace Drawer.Application.AttributeService
{
    public interface IAttributeValidationService
    {
        bool ValidateAttribute(string attr);
    }
}