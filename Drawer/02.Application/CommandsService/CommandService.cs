﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Drawer.Annotations;
using Drawer.Application;
using Drawer.Application.Core;
using Drawer.Data;
using Drawer.Data.Entities;
using Drawer.XMLGenerator;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService : ICommandService
    {
        public DataHolder DataInstance { get; private set; }
        //public IGraphGeneratorService GraphGeneratorService { get; private set; }
        public IXMLGenerator XMLGenerator { get; private set; }
        public BaseParser ConsoleCommandParser { get; private set; }

        public event EventHandler DataUpdated;

        public CommandService(
            DataHolder dataInstance, 
            //IGraphGeneratorService graphGeneratorService,
            IXMLGenerator XMLGenerator,
            BaseParser consoleCommandParser)
        {
           DataInstance = dataInstance;
           //GraphGeneratorService = graphGeneratorService;
           this.XMLGenerator = XMLGenerator;
           ConsoleCommandParser = consoleCommandParser;

            //dataInstance.PropertyChanged += OnDataChanged;
            InitCommands();
        }

        private void InitCommands()
        {
            #region MainCommands
            ConsoleCommandParser.SaveDocumentCommand += SaveDocumentCommand;
            ConsoleCommandParser.LoadDocumentCommand += LoadDocumentCommand;
            ConsoleCommandParser.ClearDataCommand += ClearDataCommand;
            ConsoleCommandParser.GenerateXMLCommand += GenerateXmlCommand;
            //ConsoleCommandParser.UpdateViewCommand += UpdateViewCommand;
            #endregion   
 
            #region EnvironmentCommands
            ConsoleCommandParser.AddEnvironmentAttributeCommand += AddEnvironmentAttributeCommand;
            ConsoleCommandParser.RemoveEnvironmentAttributeCommand += RemoveEnvironmentAttributeCommand;
            #endregion

            #region BeliefsCommands
            ConsoleCommandParser.AddBeliefsTemplateCommand += AddBeliefsTemplateCommand;
            ConsoleCommandParser.RemoveBeliefsTemplateCommand+= RemoveBeliefsTemplateCommand;
            ConsoleCommandParser.AddBeliefsAttributeCommand += AddBeliefsAttributeCommand;   
            ConsoleCommandParser.RemoveBeliefsAttributeCommand+= RemoveBeliefsAttributeCommand;
            ConsoleCommandParser.ShowBeliefsTemplatesCommand+= ShowBeliefsTemplatesCommand;

            ConsoleCommandParser.ConnectBeliefsCommand+= ConnectBeliefsCommand;
            ConsoleCommandParser.DisconnecBeliefsCommand+= DisconnectBeliefsCommand;
            #endregion

            #region StateCommands
            ConsoleCommandParser.AddStateCommand+= AddStateCommand;   
            ConsoleCommandParser.DeleteStateCommand+= RemoveStateCommand;
            ConsoleCommandParser.AddStateAttributeCommand+= AddStateAttributeCommand;
            ConsoleCommandParser.RemoveStateAttributeCommand+= RemoveStateAttributeCommand;
            ConsoleCommandParser.ShowStatesCommand+= ShowStatesCommand;

            ConsoleCommandParser.ConnectStateCommand+= ConnectStateCommand;          
            ConsoleCommandParser.DisconnectStateCommand += DisconnectStateCommand;
            #endregion

            #region PlanCommands
            ConsoleCommandParser.AddPlanCommand += AddPlanCommand;
            ConsoleCommandParser.DeletePlanCommand += RemovePlanCommand;
            ConsoleCommandParser.ShowPlansCommand += ShowPlansCommand;

            ConsoleCommandParser.ConnectPlanToAgentCommand+= ConnectPlanToAgentCommand;          
            ConsoleCommandParser.DisconnectPlanToAgentCommand += DisconnectPlanToAgentCommand;
            ConsoleCommandParser.ConnectPlanToGoalCommand += ConnectPlanToGoalCommand;
            ConsoleCommandParser.DisconnectPlanToGoalCommand += DisconnectPlanToGoalCommand;
            ConsoleCommandParser.AddExecutePlanScriptCommand += AddExecutePlanScriptCommand;
            #endregion

            #region GoalCommands
            ConsoleCommandParser.AddGoalCommand += AddGoalCommand;
            ConsoleCommandParser.DeleteGoalCommand += RemoveGoalCommand;          
            ConsoleCommandParser.ShowGoalsCommand += ShowGoalsCommand;

            ConsoleCommandParser.ConnectGoalToStateCommand += ConnectGoalToStateCommand;
            ConsoleCommandParser.DisconnectGoalToAgentCommand += DisconnectGoalFromStateCommand;
            ConsoleCommandParser.ConnectGoalToPlanCommand += ConnectGoalToPlanCommand;
            ConsoleCommandParser.DisconnectGoalToPlanCommand+= DisconnectGoalToPlanCommand;
            #endregion

            #region ProtocolCommands
            ConsoleCommandParser.AddProtocolCommand += AddProtocolCommand;
            ConsoleCommandParser.DeleteProtocolCommand+= RemoveProtocolCommand;
            ConsoleCommandParser.AddMethodToProtocolCommand+= AddMethodToProtocolCommand;
            ConsoleCommandParser.RemoveMethodToProtocolCommand += RemoveMethodToProtocolCommand;

            ConsoleCommandParser.AddClientAgentToProtocolCommand += ConnectClientAgentToProtocolCommand;
            ConsoleCommandParser.RemoveClientAgentFromProtocolCommand += DisconnectClientAgentFromProtocolCommand;
            ConsoleCommandParser.AddServerAgentToProtocolCommand += ConnectServerAgentToProtocolCommand;
            ConsoleCommandParser.RemoveServerAgentFromProtocolCommand += DisconnectServerAgentFromProtocolCommand;

            ConsoleCommandParser.AddProtocolFuncCommand += AddProtocolFunc;
            ConsoleCommandParser.AddProtocolFuncTypeCommand += AddProtocolFuncType;
            ConsoleCommandParser.AddProtocolFuncSrcPathCommand += AddProtocolFuncSrcPathCommand;
            #endregion

            #region SensorCommands
            ConsoleCommandParser.AddSensorCommand += AddSensorCommand;
            ConsoleCommandParser.RemoveSensorCommand += RemoveSensorCommand;
            ConsoleCommandParser.AddSensorScriptCommand += AddSensorScriptCommand;
            ConsoleCommandParser.RemoveSensorScriptCommand += RemoveSensorScriptCommand;

            ConsoleCommandParser.AddSensorListeningCommand += AddSensorListeningCommand;
            ConsoleCommandParser.RemoveSensorListeningCommand += RemoveSensorListeningCommand;
            ConsoleCommandParser.AddSensorToAgentCommand += ConnectSensorToAgentCommand;
            ConsoleCommandParser.RemoveSensorFromAgentCommand += DisconnectSensorFromAgentCommand;
            ConsoleCommandParser.AddSensorScriptPathCommand += AddSensorScriptPathCommand;    
            #endregion

            #region AgentCommands
            ConsoleCommandParser.AddAgentRoleCommand += AddAgentRoleCommand;
            ConsoleCommandParser.AddAgentInstCommand += AddAgentInstCommand;
            #endregion
        }


        public void SaveDocumentCommand(string filename)
        {
            var ser = new SerializerService();
            ser.SerializeObject(DataInstance, filename);
        }

        public void LoadDocumentCommand(string filename)
        {
            var ser = new SerializerService();
            DataInstance.UpdateData(ser.DeSerializeObject<DataHolder>(filename));
        }

        public void ClearDataCommand()
        {
            DataInstance.ClearData();
        }

        public void GenerateXmlCommand()
        {
            XMLGenerator.GenerateXml(DataInstance);
        }

        public void LoadScript()
        {
            var commandsSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(commandsSb, "../../../Script.gen");
            commandsSb.Replace("\n", "");
            commandsSb.Replace("\r", "");
            var commands = commandsSb.ToString().Split(';');
            foreach (var command in commands)
            {
                var com = ">" + command;
                var output = ConsoleCommandParser.ParseCommand(com.Split(' '));
            }
        }

        private AgentRole FindFirstAgentByRole(string role)
        {
            return DataInstance.AgentRoles.FirstOrDefault(agent => agent.Role == role);
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}