﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Drawer.Application.Core
{
    public interface ICommandService : INotifyPropertyChanged
    {
        void SaveDocumentCommand(string filename);
        void LoadDocumentCommand(string filename);
        void ClearDataCommand();
        void GenerateXmlCommand();
        //void UpdateViewCommand();

        bool AddEnvironmentAttributeCommand(string name, string type);
        bool RemoveEnvironmentAttributeCommand(string name);

        bool AddBeliefsTemplateCommand(string name);
        bool RemoveBeliefsTemplateCommand(string name);
        bool AddBeliefsAttributeCommand(string beliefsName, string attrName, string attrType);
        bool RemoveBeliefsAttributeCommand(string beliefsName, string attrName);
        void ShowBeliefsTemplatesCommand();
        bool ConnectBeliefsCommand(string beliefsName, string agentRole);

        bool DisconnectBeliefsCommand(string beliefsName, string agentName);

        //State
        bool AddStateCommand(string name);
        bool RemoveStateCommand(string name);
        bool AddStateAttributeCommand(string stateName, string attrName, string attrType);
        bool RemoveStateAttributeCommand(string stateName, string attrName);
        void ShowStatesCommand();

        bool ConnectStateCommand(string stateName, string agentRole);
        bool DisconnectStateCommand(string stateName, string agentRole);

        //PlanVertex
        bool AddPlanCommand(string s1);
        bool RemovePlanCommand(string s1);
        void ShowPlansCommand();

        bool ConnectPlanToAgentCommand(string s1, string s2);
        bool DisconnectPlanToAgentCommand(string s1, string s2);
        bool ConnectPlanToGoalCommand(string s1, string s2);
        bool DisconnectPlanToGoalCommand(string s1, string s2);

        //GoalVertex
        bool AddGoalCommand(string name);
        bool RemoveGoalCommand(string name);
        void ShowGoalsCommand();

        bool ConnectGoalToStateCommand(string goalName, string stateName);
        bool DisconnectGoalFromStateCommand(string s1, string s2);
        bool ConnectGoalToPlanCommand(string s1, string s2);
        bool DisconnectGoalToPlanCommand(string s1, string s2);

        //Protocol
        bool AddProtocolCommand(string s1);
        bool RemoveProtocolCommand(string s1);
        bool AddMethodToProtocolCommand(string sourceName, string returnType, string methodName, List<string> args);
        bool RemoveMethodToProtocolCommand(string sourceName, string returnType, string methodName, List<string> args);

        bool ConnectClientAgentToProtocolCommand(string s1, string s2);
        bool DisconnectClientAgentFromProtocolCommand(string s1, string s2);
        bool ConnectServerAgentToProtocolCommand(string s1, string s2);
        bool DisconnectServerAgentFromProtocolCommand(string s1, string s2);

        //sensor
        bool AddSensorCommand(string name);
        bool RemoveSensorCommand(string name);

        bool AddSensorScriptCommand(string name, string script);
        bool RemoveSensorScriptCommand(string s1, string s2);

        bool AddSensorListeningCommand(string sensorName, string environmentAttributeName);
        bool RemoveSensorListeningCommand(string sensorName, string environmentAttribute);

        bool ConnectSensorToAgentCommand(string sensorName, string agentName);
        bool DisconnectSensorFromAgentCommand(string sensorName, string agentRole);

        //agent
        bool AddAgentInstCommand(string agentName, string agentRole);
        bool AddAgentRoleCommand(string roleName, string targetState, string targetBelief);

        //event EventHandler DataUpdated;
        void LoadScript();
    }
}