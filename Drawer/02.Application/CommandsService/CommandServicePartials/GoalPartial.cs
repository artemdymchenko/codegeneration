﻿using System;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddGoalCommand(string name)
        {
            if (FindFirstGoalByName(name) != null)
                return false;   

            DataInstance.Goals.Add(new Goal(name));
            return true;
        }

        public bool RemoveGoalCommand(string name)
        {
            return DataInstance.Goals.Remove(FindFirstGoalByName(name));
        }

        public void ShowGoalsCommand()
        {
        }

        public bool ConnectGoalToStateCommand(string goalName, string stateName)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            var targetState = FindFirstStateByName(stateName);
            if (targetState == null) return false;

            targetGoal.AssociatedStateName = targetState.Name;
            return true;
        }

        public bool DisconnectGoalFromStateCommand(string goalName, string agentRole)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            targetGoal.AssociatedStateName = null;
            return true;
        }

        public bool ConnectGoalToPlanCommand(string goalName, string planName)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetGoal.ConnectToPlan(targetPlan);
            return true;
        }

        public bool DisconnectGoalToPlanCommand(string goalName, string planName)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetGoal.DisonnectFromPlan(targetPlan);
            return true;
        }

        //helping method
        private Goal FindFirstGoalByName(string name)
        {
            return DataInstance.Goals.FirstOrDefault(goal => goal.Name == name);
        }
    }
}