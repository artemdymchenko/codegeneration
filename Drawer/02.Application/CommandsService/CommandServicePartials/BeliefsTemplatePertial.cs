﻿using System;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddBeliefsTemplateCommand(string name)
        {
            if (FindFirstBeliefsTemplateByName(name) != null)
                return false;

            DataInstance.BeliefsTemplates.Add(new BeliefsTemplate(name));
            return true;
        }

        public bool RemoveBeliefsTemplateCommand(string name)
        {
            return DataInstance.BeliefsTemplates.Remove(FindFirstBeliefsTemplateByName(name));
        }

        public bool AddBeliefsAttributeCommand(string beliefsName, string attrName, string attrType)
        {
            var targetBelief = FindFirstBeliefsTemplateByName(beliefsName);
            if (targetBelief == null) return false;

            try
            {
                if (!targetBelief.ContainsKey(attrName))
                {
                    targetBelief.Add(attrType, attrName);
                    return true;
                }
            }
            catch (ArgumentException ex)
            {
                return false;
            }

            return false;
        }

        public bool RemoveBeliefsAttributeCommand(string beliefsName, string attrName)
        {
            var targetBelief = FindFirstBeliefsTemplateByName(beliefsName);
            if (targetBelief == null) return false;

            return targetBelief.Remove(attrName);
        }

        public void ShowBeliefsTemplatesCommand()
        {
        }

        public bool ConnectBeliefsCommand(string beliefsName, string agentRole)
        {
            var targetBelief = FindFirstBeliefsTemplateByName(beliefsName);
            if (targetBelief == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetBelief.AssociatedAgentRoleGuid = targetAgent.Guid;
            return true;
        }

        public bool DisconnectBeliefsCommand(string beliefsName, string agentName)
        {
            var targetBelief = FindFirstBeliefsTemplateByName(beliefsName);
            if (targetBelief == null) return false;

            targetBelief.AssociatedAgentRoleGuid = Guid.Empty;
            return true;
        }

        //helping method
        private BeliefsTemplate FindFirstBeliefsTemplateByName(string name)
        {
            return DataInstance.BeliefsTemplates.FirstOrDefault(beliefsTemplate => beliefsTemplate.Name == name);
        }
    }
}