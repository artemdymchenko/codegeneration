﻿using System;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService 
    {
        //public bool AddAgentCommand(string name)
        //{
        //    if (FindFirstAgentRoleByName(name) != null)
        //        return false;

        //    DataInstance.AgentRoles.Add(new AgentRole(name));
        //    return true;
        //}

        //public bool RemoveAgentCommand(string name)
        //{
        //    return DataInstance.AgentRoles.Remove(FindFirstAgentRoleByName(name));
        //}

        //helping method
        private AgentRole FindFirstAgentRoleByName(string name)
        {
            return DataInstance.AgentRoles.FirstOrDefault(agent => agent.Role == name);
        }

        public bool AddAgentRoleCommand(string roleName, string targetState, string targetBelief)
        {
            if (FindFirstAgentRoleByName(roleName) != null)
                return false;

            var state = FindFirstStateByName(targetState);
            if (state == null) return false;
            var belief = FindFirstBeliefsTemplateByName(targetBelief);
            if (belief == null) return false;

            var newAgentRole = new AgentRole(roleName);
            DataInstance.AgentRoles.Add(newAgentRole);
            state.AssociatedAgentRoleGuid = newAgentRole.Guid;
            belief.AssociatedAgentRoleGuid = newAgentRole.Guid;

            return true;
        }
    }
}