﻿using System;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddStateCommand(string name)
        {
            if (FindFirstStateByName(name) != null)
                return false;

            DataInstance.StateTemplates.Add(new StateTemplate(name));
            return true;
        }

        public bool RemoveStateCommand(string name)
        {
            return DataInstance.StateTemplates.Remove(FindFirstStateByName(name));
        }

        public bool AddStateAttributeCommand(string stateName, string attrName, string attrType)
        {
            var targetState = FindFirstStateByName(stateName);
            if (targetState == null) return false;

            try
            {
                if (!targetState.ContainsKey(attrType))
                {
                    targetState.Add(attrName, attrType);
                    return true;
                }
            }
            catch (ArgumentException ex)
            {
                return false;
            }

            return false;
        }

        public bool RemoveStateAttributeCommand(string stateName, string attrName)
        {
            var targetState = FindFirstStateByName(stateName);
            if (targetState == null) return false;

            return targetState.Remove(attrName);
 
        }

        public void ShowStatesCommand()
        {
        }

        public bool ConnectStateCommand(string stateName, string agentRole)
        {
            var targetState = FindFirstStateByName(stateName);
            if (targetState == null) return false;

            var targetAgent = FindFirstAgentRoleByName(agentRole);
            if (targetAgent == null) return false;

            targetState.AssociatedAgentRoleGuid = targetAgent.Guid;
            return true;
        }

        public bool DisconnectStateCommand(string stateName, string agentRole)
        {
            var targetState = FindFirstStateByName(stateName);
            if (targetState == null) return false;

            var targetAgent = FindFirstAgentRoleByName(agentRole);
            if (targetAgent == null) return false;

            targetState.AssociatedAgentRoleGuid = Guid.Empty;
            return true;
        }

        //Helping
        private StateTemplate FindFirstStateByName(string name)
        {
            return DataInstance.StateTemplates.FirstOrDefault(sensor => sensor.Name == name);
        }
    }
}