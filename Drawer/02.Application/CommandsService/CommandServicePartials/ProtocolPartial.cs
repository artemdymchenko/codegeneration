﻿using System;
using System.Collections.Generic;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddProtocolCommand(string name)
        {
            if (FindFirstProtocolByName(name) != null)
                return false;

            DataInstance.Protocols.Add(new Protocol(name));
            return true;
        }

        public bool RemoveProtocolCommand(string name)
        {
            return DataInstance.Protocols.Remove(FindFirstProtocolByName(name));
        }
        //TODO:add feature
        public bool AddMethodToProtocolCommand(string sourceName, string returnType, string methodName, List<string> args)
        {
            throw new NotImplementedException();
        }
        //TODO:add feature
        public bool RemoveMethodToProtocolCommand(string sourceName, string returnType, string methodName, List<string> args)
        {
            throw new NotImplementedException();
        }

        public bool ConnectClientAgentToProtocolCommand(string protocolName, string agentRole)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetProtocol.Client = targetAgent.Guid;
            return true;
        }

        public bool DisconnectClientAgentFromProtocolCommand(string protocolName, string agentRole)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetProtocol.Server = Guid.Empty;
            return true;
        }

        public bool ConnectServerAgentToProtocolCommand(string protocolName, string agentRole)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetProtocol.Server = targetAgent.Guid;
            return true;
        }

        public bool DisconnectServerAgentFromProtocolCommand(string protocolName, string agentRole)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetProtocol.Server = Guid.Empty;
            return true;
        }

        private bool AddProtocolFunc(string protocolName, string protocolFuncName)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            targetProtocol.ProtocolMethods.Add(new ProtocolMethod(protocolFuncName));
            return true;
        }

        private bool AddProtocolFuncType(string protocolName, string methodName, string type)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            ProtocolMethod targetMethod = null;
            foreach (var protocolMethod in targetProtocol.ProtocolMethods)
            {
                if (protocolMethod.MethodName == methodName)
                    targetMethod = protocolMethod;
            }

            if (targetMethod == null) return false;

            targetMethod.Types.Add(type);
            return true;
        }

        private bool AddProtocolFuncSrcPathCommand(string protocolName, string methodName, string path)
        {
            var targetProtocol = FindFirstProtocolByName(protocolName);
            if (targetProtocol == null) return false;

            ProtocolMethod targetMethod = null;
            foreach (var protocolMethod in targetProtocol.ProtocolMethods)
            {
                if (protocolMethod.MethodName == methodName)
                    targetMethod = protocolMethod;
            }

            if (targetMethod == null) return false;
            targetMethod.Source = path;
            return true;
        }

        //Helping
        Protocol FindFirstProtocolByName(string name)
        {
            return DataInstance.Protocols.FirstOrDefault(protocol => protocol.Name == name);
        }

    }
}