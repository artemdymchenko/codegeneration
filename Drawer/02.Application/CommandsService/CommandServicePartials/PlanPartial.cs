﻿using System;
using System.Linq;
using Drawer.Annotations;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddPlanCommand(string name)
        {
            if (FindFirstPlanByName(name) != null)
                return false;

            DataInstance.Plans.Add(new Plan(name));
            return true;
        }

        public bool RemovePlanCommand(string name)
        {
            return DataInstance.Plans.Remove(FindFirstPlanByName(name));
        }

        public void ShowPlansCommand()
        {
        }

        public bool ConnectPlanToAgentCommand(string planName, string agentRole)
        {
            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            var targetAgent = FindFirstAgentByRole(agentRole);
            if (targetAgent == null) return false;

            targetPlan.AssociatedAgentGuid = targetAgent.Guid;
            return true;
        }

        public bool DisconnectPlanToAgentCommand(string planName, string agentRole)
        {
            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetPlan.AssociatedAgentGuid = Guid.Empty;
            return true;
        }

        public bool ConnectPlanToGoalCommand(string planName, string goalName)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetPlan.ConnectToAGoal(targetGoal);
            return true;
        }

        public bool DisconnectPlanToGoalCommand(string planName, string goalName)
        {
            var targetGoal = FindFirstGoalByName(goalName);
            if (targetGoal == null) return false;

            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetPlan.DisconnectFromGoal(targetGoal);
            return true;
        }

        private bool AddExecutePlanScriptCommand(string planName, string path)
        {
            var targetPlan = FindFirstPlanByName(planName);
            if (targetPlan == null) return false;

            targetPlan.ExecuteScriptPath = path;
            return true;
        }

        //helping method
        private Plan FindFirstPlanByName(string name)
        {
            return DataInstance.Plans.FirstOrDefault(plan => plan.Name == name);
        }


    }
}