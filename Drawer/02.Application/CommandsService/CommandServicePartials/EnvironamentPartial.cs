﻿using System;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddEnvironmentAttributeCommand(string type, string name)
        {
        //    DataInstance.Environment.Add(name, type);
        //    return true;

            //var targetBelief = FindFirstBeliefsTemplateByName(beliefsName);
            //if (targetBelief == null) return false;

            try
            {
                if (!DataInstance.Environment.ContainsKey(name))
                {
                    DataInstance.Environment.Add(type, name);
                    return true;
                }
            }
            catch (ArgumentException ex)
            {
                return false;
            }

            return false;
        }

        public bool RemoveEnvironmentAttributeCommand(string name)
        {
            return DataInstance.Environment.Remove(name);
        }
    }
}