﻿using System;
using System.Linq;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddSensorCommand(string name)
        {
            //if (FindFirstSensorByName(name) != null)
            //    return false;

            DataInstance.Sensors.Add(new Sensor(name));
            return true;
        }

        public bool RemoveSensorCommand(string name)
        {
            return DataInstance.Sensors.Remove(FindFirstSensorByName(name));
        }
        //TODO:add feature
        public bool AddSensorScriptCommand(string name, string script)
        {
            throw new NotImplementedException();
        }
        //TODO:add feature
        public bool RemoveSensorScriptCommand(string s1, string s2)
        {
            throw new NotImplementedException();
        }

        public bool AddSensorListeningCommand(string sensorName, string environmentAttributeName)
        {
            var targetSensor = FindFirstSensorByName(sensorName);
            if (targetSensor == null) return false;

            if (DataInstance.Environment.ContainsKey(environmentAttributeName))
            {
                targetSensor.TargetEnvironment = environmentAttributeName;
                return true;
            }
            return false;
        }

        public bool RemoveSensorListeningCommand(string sensorName, string environmentAttribute)
        {
            var targetSensor = FindFirstSensorByName(sensorName);
            if (targetSensor == null) return false;

            if (!DataInstance.Environment.ContainsKey(environmentAttribute))
                return false;

            targetSensor.TargetEnvironment = "";
            return true;
        }

        public bool ConnectSensorToAgentCommand(string sensorName, string agentName)
        {
            var targetSensor = FindFirstSensorByName(sensorName);
            if (targetSensor == null) return false;

            var targetAgent = FindFirstAgentInstanceByName(agentName);
            if (targetAgent == null) return false;

            targetSensor.AssociatedAgentInstanceGuid = targetAgent.Guid;
            return true;
        }

        public bool DisconnectSensorFromAgentCommand(string sensorName, string agentRole)
        {
            var targetSensor = FindFirstSensorByName(sensorName);
            if (targetSensor == null) return false;

            var targetAgent = FindFirstAgentRoleByName(agentRole);
            if (targetAgent == null) return false;

            targetSensor.AssociatedAgentInstanceGuid = Guid.Empty;
            return true;
        }

        private bool AddSensorScriptPathCommand(string sensorName, string path)
        {
            var targetSensor = FindFirstSensorByName(sensorName);
            if (targetSensor == null) return false;

            targetSensor.Script = path;
            return true;
        }

        //Helping
        private Sensor FindFirstSensorByName(string name)
        {
            return DataInstance.Sensors.FirstOrDefault(sensor => sensor.Name == name);
        }
    }
}