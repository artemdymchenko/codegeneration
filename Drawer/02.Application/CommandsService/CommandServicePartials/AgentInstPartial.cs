﻿using System.Linq;
using Drawer.Data.Entities;

namespace Drawer.Application.CommandsService
{
    public partial class CommandService
    {
        public bool AddAgentInstCommand(string agentName, string agentRole)
        {
            if (FindFirstAgentInstanceByName(agentName) != null)
                return false;

            var role = FindFirstAgentByRole(agentRole);
            if (role == null) return false;

            var newAgentInstance = new AgentInstance(role.Guid, agentName);
            DataInstance.AgentInstances.Add(newAgentInstance);
            return true;
        }

        private AgentInstance FindFirstAgentInstanceByName(string name)
        {
            return DataInstance.AgentInstances.FirstOrDefault(agent => agent.Name == name);
        }
    }
}