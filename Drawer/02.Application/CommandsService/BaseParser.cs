﻿using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace Drawer.Application.CommandsService
{
    public abstract class BaseParser
    {
        protected BaseParser ()
        {
            AddProtocolFuncCommand = null;
            AddProtocolFuncTypeCommand = null;
        }
        public abstract string ParseCommand(string[] command);

        //delegates
        public delegate void StringMethod(string filename);
        public delegate bool StringMethodOfBool(string filename);
        public delegate void TwoStringsMethod(string type, string name);
        public delegate bool TwoStringsMethodOfBool(string type, string name);
        public delegate void ThreeStringsMethod(string s1, string s2, string s3);
        public delegate bool ThreeStringsMethodOfBool(string s1, string s2, string s3);
        public delegate void FunctionMethod(string sourceName, string returnType, string methodName, List<string> list);
        public delegate bool FunctionMethodOfBool(string sourceName, string returnType, string methodName, List<string> list);
        public delegate void VoidMethod();

        //console commands
        public StringMethod SaveDocumentCommand;
        public StringMethod LoadDocumentCommand;
        public VoidMethod ClearDataCommand;
        public VoidMethod GenerateXMLCommand;
        public VoidMethod UpdateViewCommand;

        //_environment
        public TwoStringsMethodOfBool AddEnvironmentAttributeCommand;
        public StringMethodOfBool RemoveEnvironmentAttributeCommand;

        //Beiefs
        public StringMethodOfBool AddBeliefsTemplateCommand;
        public StringMethodOfBool RemoveBeliefsTemplateCommand;
        public ThreeStringsMethodOfBool AddBeliefsAttributeCommand;
        public TwoStringsMethodOfBool RemoveBeliefsAttributeCommand;
        public VoidMethod ShowBeliefsTemplatesCommand;

        public TwoStringsMethodOfBool ConnectBeliefsCommand;
        public TwoStringsMethodOfBool DisconnecBeliefsCommand;

        //State
        public StringMethodOfBool AddStateCommand;
        public StringMethodOfBool DeleteStateCommand;
        public ThreeStringsMethodOfBool AddStateAttributeCommand;
        public TwoStringsMethodOfBool RemoveStateAttributeCommand;
        public VoidMethod ShowStatesCommand;

        public TwoStringsMethodOfBool ConnectStateCommand;
        public TwoStringsMethodOfBool DisconnectStateCommand;

        public Func<string, string, bool> AddStateAttribCommand;

        //PlanVertex
        public StringMethodOfBool AddPlanCommand;
        public StringMethodOfBool DeletePlanCommand;
        public VoidMethod ShowPlansCommand;

        public TwoStringsMethodOfBool ConnectPlanToAgentCommand;
        public TwoStringsMethodOfBool DisconnectPlanToAgentCommand;
        public TwoStringsMethodOfBool ConnectPlanToGoalCommand;
        public TwoStringsMethodOfBool DisconnectPlanToGoalCommand;
        public Func<string, string, bool> AddExecutePlanScriptCommand;

        //GoalVertex
        public StringMethodOfBool AddGoalCommand;
        public StringMethodOfBool DeleteGoalCommand;
        public VoidMethod ShowGoalsCommand;

        public TwoStringsMethodOfBool ConnectGoalToStateCommand;
        public TwoStringsMethodOfBool DisconnectGoalToAgentCommand;
        public TwoStringsMethodOfBool ConnectGoalToPlanCommand;
        public TwoStringsMethodOfBool DisconnectGoalToPlanCommand;

        //Protocol
        public StringMethodOfBool AddProtocolCommand;
        public StringMethodOfBool DeleteProtocolCommand;
        public FunctionMethodOfBool AddMethodToProtocolCommand;
        public FunctionMethodOfBool RemoveMethodToProtocolCommand;

        public TwoStringsMethodOfBool AddClientAgentToProtocolCommand;
        public TwoStringsMethodOfBool RemoveClientAgentFromProtocolCommand;
        public TwoStringsMethodOfBool AddServerAgentToProtocolCommand;
        public TwoStringsMethodOfBool RemoveServerAgentFromProtocolCommand;

        public Func<string, string, bool> AddProtocolFuncCommand;
        public Func<string, string, string, bool> AddProtocolFuncTypeCommand;
        public Func<string, string, string, bool> AddProtocolFuncSrcPathCommand;

        //sensor
        public StringMethodOfBool AddSensorCommand;
        public StringMethodOfBool RemoveSensorCommand;

        public TwoStringsMethodOfBool AddSensorScriptCommand;
        public TwoStringsMethodOfBool RemoveSensorScriptCommand;

        public TwoStringsMethodOfBool AddSensorListeningCommand;
        public TwoStringsMethodOfBool RemoveSensorListeningCommand;

        public TwoStringsMethodOfBool AddSensorToAgentCommand;
        public TwoStringsMethodOfBool RemoveSensorFromAgentCommand;
        public Func<string, string, bool> AddSensorScriptPathCommand;

        //agent
        //public StringMethodOfBool AddAgentCommand;
        //public StringMethodOfBool RemoveAgentCommand;
        public Func<string, string, string, bool> AddAgentRoleCommand;
        public Func<string, string, bool> AddAgentInstCommand;
    }
}