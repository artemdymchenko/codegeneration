﻿using Drawer.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Drawer.Application.CommandsService
{
    public class ConsoleCommandParser : BaseParser
    {
        private delegate string Command(string[] str);
        private Dictionary<string, Command> _actions;

        public const string wrongNumberOfAttributes = "Wrong number of attributes!";
        public const string unknownAttributeType = "Unknown attribute type";
        public const string operationSucceed = "Operation succeed";
        public const string operationFailed = "Operation failed";

        public ConsoleCommandParser()
        {
            #region Commands Binding
            _actions = new Dictionary<string, Command> {
                { "add_attr", EnvironmentAttributeName },
                { "remove_attr", RemoveEnvironmentAttribute },

                { "add_beliefs", AddBeliefs },
                { "remove_beliefs", RemoveBeliefs },
                { "add_beliefs_attr", AddBeliefsAttr },
                { "remove_beliefs_attr", RemoveBeliefsAttr },
                { "show_beliefs", ShowBeliefs },
                { "conn_beliefs", ConnectBeliefs },
                { "disconn_beliefs", DisconnectBeliefs },

                { "add_state", AddState },
                { "remove_state", RemoveState },
                { "add_state_attr", AddStateAttr },
                { "remove_state_attr", RemoveStateAttr },
                { "show_states", ShowStatrs },
                { "conn_state", ConnectState },
                { "disconn_state", DisconnectState },

                { "add_plan", AddPlan },
                { "remove_plan", RemovePlan },
                { "show_plans", ShowPlans },
                { "conn_plan_ag", ConnectPlanToAgent },
                { "disconn_plan_ag", DiscoonectPlanFormAgent },
                { "conn_plan_goal", ConnectPlanToGoal },
                { "disconn_plan_conn_plan_goal", DisconnectPlanFromGoal },
                { "add_execute_plan_script", AddExecutePlanScript },

                { "add_goal", AddGoal },
                { "remove_goal", RemoveGoal },
                { "show_goals", ShowGoals },
                { "conn_goal_state", ConnectGoalToState },
                { "disconn_goal_state", DisconnectGoalFromState },
                { "conn_goal_plan", ConnectGoalToPlan },
                { "disconn_goal_plan", DisconnectGoalFromPlan },

                { "add_protocol", AddProtocol },
                { "remove_protocol", RemoveProtocol },
                { "add_client_to_protocol", AddClientToProtocol },
                { "remove_client_from_protocol", RemoveClientRemovedFromProtocol },
                { "add_server_to_protocol", AddServerToProtocol },
                { "remove_server_from_protocol", RemoveServerFromProtocol },
                { "add_protocol_func", AddProtocolFunc },
                { "add_protocol_func_type", AddProtocolFuncType },
                { "add_protocol_src_path", AddProtocolFuncSrcPath },

                { "add_sensor", AddSensor },
                { "remove_sensor", RemoveSensor },
                { "add_sensor_listener_to", AddSensorListenrTo },
                { "remove_sensor_listener_to", RemoveSensorListenerTo },
                { "add_sensor_to_ag", AddSensorToAgent },
                { "remove_sensor_from_ag", RemoveSensorFromAgent },
                { "add_sensor_script_path", AddSensorScriptPath },

                {"add_agent_role", AddAgentRole},
                {"add_agent_inst", AddAgentInst},
            };
            #endregion
        }

        private string AddAgentInst(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddAgentInstCommand != null && AddAgentInstCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddAgentRole(string[] command)
        {
            if (command.Length != 4)
            {
                return wrongNumberOfAttributes;
            }
            if (AddAgentRoleCommand != null && AddAgentRoleCommand(command[1], command[2], command[3]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        #region Commands Methods

        private string RemoveSensorFromAgent(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveSensorFromAgentCommand != null && RemoveSensorFromAgentCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddSensorScriptPath(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddSensorScriptPathCommand != null && AddSensorScriptPathCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }
        
        private string AddSensorToAgent(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddSensorToAgentCommand != null && AddSensorToAgentCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string RemoveSensorListenerTo(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveSensorListeningCommand != null && RemoveSensorListeningCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddSensorListenrTo(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            
            if (AddSensorListeningCommand != null && AddSensorListeningCommand(command[1], command[2]))
                return operationSucceed;
            return operationFailed;
        }


        private string RemoveSensor(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveSensorCommand != null && RemoveSensorCommand(command[1]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddSensor(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (AddSensorCommand != null && AddSensorCommand(command[1])) return operationSucceed;
            return operationFailed;
        }

        private string RemoveServerFromProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveServerAgentFromProtocolCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddServerToProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddServerAgentToProtocolCommand(command[1], command[2])) return operationSucceed;
            return operationFailed;
        }

        private string RemoveClientRemovedFromProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveClientAgentFromProtocolCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string RemoveClientFromProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveClientAgentFromProtocolCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string RemoveClientFormProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveClientAgentFromProtocolCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddClientToProtocol(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddClientAgentToProtocolCommand(command[1], command[2]))
                return operationSucceed;
            return operationFailed;
        }

        private string RemoveProtocol(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (DeleteProtocolCommand(command[1]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddProtocol(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (AddProtocolCommand(command[1]))
                return operationSucceed;
            return operationFailed;
        }

        private string DisconnectGoalFromPlan(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (DisconnectGoalToPlanCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ConnectGoalToPlan(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (ConnectGoalToPlanCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string DisconnectGoalFromState(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (DisconnectGoalToAgentCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ConnectGoalToState(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (ConnectGoalToStateCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ShowGoals(string[] command)
        {
            if (command.Length != 1)
            {
                return wrongNumberOfAttributes;
            }
            ShowGoalsCommand();
            return "Goals were shown";
        }

        private string RemoveGoal(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (DeleteGoalCommand(command[1]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddGoal(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (AddGoalCommand(command[1]))
                return operationSucceed;
            return operationFailed;
        }

        private string DisconnectPlanFromGoal(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (DisconnectPlanToGoalCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddExecutePlanScript(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddExecutePlanScriptCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        
        private string ConnectPlanToGoal(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (ConnectPlanToGoalCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string DiscoonectPlanFormAgent(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (DisconnectPlanToAgentCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ConnectPlanToAgent(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (ConnectPlanToAgentCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ShowPlans(string[] command)
        {
            if (command.Length != 1)
            {
                return wrongNumberOfAttributes;
            }
            ShowPlansCommand();
            return "Plans were shown";
        }

        private string RemovePlan(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (DeletePlanCommand(command[1]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddPlan(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (AddPlanCommand(command[1]))
                return operationSucceed;
            return operationFailed;
        }

        private string DisconnectState(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (DisconnectStateCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ConnectState(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (ConnectStateCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string ShowStatrs(string[] command)
        {
            if (command.Length != 1)
            {
                return wrongNumberOfAttributes;
            }
            ShowStatesCommand();
            return "States were shown";
        }

        private string RemoveStateAttr(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (RemoveStateAttributeCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddStateAttr(string[] command)
        {
            if (command.Length != 4)
            {
                return wrongNumberOfAttributes;
            }
            if (AddStateAttributeCommand(command[1], command[2], command[3]))
                return operationSucceed;
            return operationFailed;
        }

        private string RemoveState(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (DeleteStateCommand(command[1]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddState(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }
            if (AddStateCommand(command[1]))
                return operationSucceed;
            return operationFailed;
        }

        private string DisconnectBeliefs(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }

            if (DisconnecBeliefsCommand(command[1], command[2]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string ConnectBeliefs(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }

            if (ConnectBeliefsCommand(command[1], command[2]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string ShowBeliefs(string[] command)
        {
            if (command.Length != 1)
            {
                return wrongNumberOfAttributes;
            }
            ShowBeliefsTemplatesCommand();

            return "Beliefs were shown";
        }

        private string RemoveBeliefsAttr(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }

            if (RemoveBeliefsAttributeCommand(command[1], command[2]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string AddBeliefsAttr(string[] command)
        {
            if (command.Length != 4)
            {
                return wrongNumberOfAttributes;
            }

            if (AddBeliefsAttributeCommand(command[1], command[2], command[3]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string RemoveBeliefs(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }

            if (RemoveBeliefsTemplateCommand(command[1]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string AddBeliefs(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }

            if (AddBeliefsTemplateCommand(command[1]))
                return operationSucceed;
            else
                return operationFailed;
        }

        private string RemoveEnvironmentAttribute(string[] command)
        {
            if (command.Length != 2)
            {
                return wrongNumberOfAttributes;
            }

            if (!RemoveEnvironmentAttributeCommand(command[1]))
                return unknownAttributeType;

            return operationFailed;
        }

        private string EnvironmentAttributeName(string[] command)
        {
            if (command.Length != 3)
                return wrongNumberOfAttributes;

            if (!Resolver.Instance.AttributeValidationService.ValidateAttribute(command[1]))
                return unknownAttributeType;

            if (AddEnvironmentAttributeCommand != null && AddEnvironmentAttributeCommand(command[1], command[2]))
                return operationSucceed;

            return operationFailed;
        }

        private string AddProtocolFunc(string[] command)
        {
            if (command.Length != 3)
            {
                return wrongNumberOfAttributes;
            }
            if (AddProtocolFuncCommand != null && AddProtocolFuncCommand(command[1], command[2]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddProtocolFuncType(string[] command)
        {
            if (command.Length != 4)
            {
                return wrongNumberOfAttributes;
            }
            if (AddProtocolFuncTypeCommand != null && AddProtocolFuncTypeCommand(command[1], command[2], command[3]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        private string AddProtocolFuncSrcPath(string[] command)
        {
            if (command.Length != 4)
            {
                return wrongNumberOfAttributes;
            }
            if (AddProtocolFuncSrcPathCommand != null && AddProtocolFuncSrcPathCommand(command[1], command[2], command[3]))
            {
                return operationSucceed;
            }
            return operationFailed;
        }

        #endregion

        public override string ParseCommand(string[] command)
        {
            Command action;
            if (_actions.TryGetValue(command[0].Substring(1), out action))
            {
                var list = command.ToList();
                list.RemoveAll(x => x.Equals(""));
                return action(list.ToArray());
            }

            return "Command not found";
        }
    }
}