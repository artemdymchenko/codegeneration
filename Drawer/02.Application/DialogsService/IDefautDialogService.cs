﻿using Microsoft.Win32;

namespace Drawer.Presentation.Dialogs
{
    public interface IDefautDialogService
    {
        OpenFileDialog OpenFileDialog();
        SaveFileDialog SaveFileDialog();
    }
}