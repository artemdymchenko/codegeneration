﻿using System.IO;
using Microsoft.Win32;

namespace Drawer.Presentation.Dialogs
{
    public class DefaultDialogsService : IDefautDialogService
    {
        public OpenFileDialog OpenFileDialog()
        {
            var currentFolder = Directory.GetCurrentDirectory();

            var dlg = new OpenFileDialog
            {
                DefaultExt = ".gen",
                Filter =
                    "GEN Files (*.gen)|*.gen",
                InitialDirectory = currentFolder
            };
            return dlg;
        }

        public SaveFileDialog SaveFileDialog()
        {
            var currentFolder = Directory.GetCurrentDirectory();

            var dlg = new SaveFileDialog
            {
                DefaultExt = ".gen",
                Filter =
                    "GEN Files (*.gen)|*.gen",
                InitialDirectory = currentFolder
            };
            return dlg;
        }
    }
}