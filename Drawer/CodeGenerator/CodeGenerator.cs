﻿using System;
using System.Collections.Generic;
using System.IO;
using CodeGenerator.ClassGenerators;
using CodeGenerator.XMLEntities;

namespace CodeGenerator
{
    public class CodeGenerator
    {
        public List<string> GeneratedFiles { get; private set; }

        public CodeGenerator()
        {
            GeneratedFiles = new List<string>();
        }

        private void CreateSourceFile(string environmentClassSourceFilePath, string environmentClassSource)
        {
            var directoryInfo = (new FileInfo(environmentClassSourceFilePath)).Directory;
            if (directoryInfo != null)
                directoryInfo.Create();

            var file = new StreamWriter(environmentClassSourceFilePath);
            //Console.WriteLine(environmentClassSource);
            file.WriteLine(environmentClassSource);
            file.Close();
            GeneratedFiles.Add(environmentClassSourceFilePath);
        }

        public void GenerateEnvironmentClassSourceFile(string destinationFolder, string namespaceName, List<XMLAttribute> attributes)
        {
            CreateSourceFile(destinationFolder + EnvironmentClassGenerator.ClassName,
                EnvironmentClassGenerator.GenerateSource(namespaceName, attributes));
        }

        public void GenerateBaseAgentSourceFile(string destinationFolder, string namespaceName)
        {
            CreateSourceFile(destinationFolder + @"Agents\" + BaseAgentClassGenerator.ClassName,
                BaseAgentClassGenerator.GenerateSource(namespaceName));
        }

        public void GenerateBlackBoardClassSourceFile(string destinationFolder, string namespaceName)
        {
            CreateSourceFile(destinationFolder + BlackBoardClassGenerator.ClassName,
                BlackBoardClassGenerator.GenerateSource(namespaceName));
        }

        public void GenerateProtocolClassesSourceFiles(string destinationFolder, string namespaceName, List<XMLProtocol> protocols)
        {
            var sources = ProtocolClassGenerator.GenerateSources(namespaceName, protocols);
            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"Protocols\" + ProtocolClassGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            ProtocolClassGenerator.ClassNames = new List<string>();
        }

        public void GeneratePlanClassSourceFiles(string destinationFolder, string namespaceName, XmlParser xmlParser)
        {
            var sources = PlanClassGenerator.GenerateSources(namespaceName, xmlParser);
            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"Plans\" + PlanClassGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            PlanClassGenerator.ClassNames = new List<string>();
        }

        public void GenerateGoalClassSourceFiles(string destinationFolder, string namespaceName, List<XMLPlan> plans, List<XMLGoal> goals)
        {
            var sources = GoalClassGenerator.GenerateSources(namespaceName, plans, goals);

            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"Goals\" + GoalClassGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            GoalClassGenerator.ClassNames = new List<string>();
        }

        public void GenerateBeliefsInterfaceSourceFiles(string destinationFolder, string namespaceName, List<XMLNamedAttributeList> beliefs)
        {
            var sources = BeliefsGenerator.GenerateSources(namespaceName, beliefs);

            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"Beliefs\" + BeliefsGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            BeliefsGenerator.ClassNames = new List<string>();
        }

        public void GenerateAgentSourceFiles(string destinationFolder, string namespaceName, XmlParser xmlParser)
        {
            var sources = AgentGenerator.GenerateSources(namespaceName, xmlParser);

            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"Agents\" + AgentGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            AgentGenerator.ClassNames = new List<string>();
        }

        public void GenerateBaseSensor(string destinationFolder, string namespaceName)
        {
            CreateSourceFile(destinationFolder + @"Sensors\" + BaseSensorGenerator.ClassName,
                BaseSensorGenerator.GenerateSource(namespaceName));
        }

        public void GenerateStateSourceFiles(string destinationFolder, string namespaceName, List<XMLState> states, List<XMLAgentRole> roles)
        {
            var sources = StatesGenerator.GenerateSources(namespaceName, states);

            for (var i = 0; i < sources.Count; ++i)
            {
                CreateSourceFile(destinationFolder + @"States\" + StatesGenerator.ClassNames[i] + ".cs", sources[i]);
            }
            StatesGenerator.ClassNames = new List<string>();
        }
    }
}