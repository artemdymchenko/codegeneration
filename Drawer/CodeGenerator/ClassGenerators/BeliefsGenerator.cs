﻿using System.Collections.Generic;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;
using CodeGenerator.XMLEntities;

namespace CodeGenerator.ClassGenerators
{
    public class BeliefsGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "BeliefsTemplate.tmpl";
        public static List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, List<XMLNamedAttributeList> beliefs)
        {
            var sources = new List<string>();
            foreach (var xmlBelief in beliefs)
            {
                var beliefSb = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(beliefSb, TemplateFile);
                beliefSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                beliefSb.Replace(BasicAnnotations.ClassName, xmlBelief.Name);
                ClassNames.Add(xmlBelief.Name);

                var attributesSb = new StringBuilder();
                var attributesInitSb = new StringBuilder();
                foreach (var xmlAttribute in xmlBelief.Attributes)
                {
                    var type = xmlAttribute.Type;
                    var name = xmlAttribute.Name;
                    if (type == null || name == null)
                        continue;

                    if (xmlAttribute.Type.Contains("!"))
                    {
                        var v = xmlAttribute.Type.Split('!');
                        type = v[0] + "<" + v[1] + ">";
                    }
                    string attr;
                    string attrInit;
                    switch (type)
                    {
                        case "string":
                            attrInit = name + @" = """";" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        case "int":
                            attrInit = name + @" = 0;" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        case "bool":
                            attrInit = name + @" = false;" + "\n\t\t\t";
                            attr = "public " + type + " " + name + ";\n\t\t";
                            break;
                        default:
                            attrInit = name + @" = new " + type + "();\n\t\t\t";
                            attr = "public " + type + " " + name + " { get; set; }\n\t\t";
                            break;
                    }
                    attributesSb.Append(attr);
                    attributesInitSb.Append(attrInit);
                }
                beliefSb.Replace("[attrs]", attributesSb.ToString());
                beliefSb.Replace("[attrsInit]", attributesInitSb.ToString());
                sources.Add(beliefSb.ToString());
            }
            return sources;
        } 
    }
}