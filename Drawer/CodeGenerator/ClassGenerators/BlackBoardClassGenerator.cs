﻿using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public class BlackBoardClassGenerator
    {
        private static readonly string BlackBoardTemplateFilePath = Paths.TemplatesPath + "BlackBoardTemplate.tmlp";

        public static readonly string ClassName = "BlackBoard.cs";

        public static string GenerateSource(string namespaceName)
        {
            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, BlackBoardTemplateFilePath);

            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);

            return mainSb.ToString();
        }
    }
}