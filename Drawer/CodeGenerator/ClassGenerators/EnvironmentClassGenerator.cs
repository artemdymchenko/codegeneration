﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public static class EnvironmentClassGenerator
    {
        private static readonly string EnvironmentTemplateFilePath = Paths.TemplatesPath + "EnvironmentTemplate.tmlp";
        private static readonly string EnvironmentAttributeTemplateFilePath = Paths.TemplatesPath + "EnvironmentAttributeTemplate.tmlp";

        //TODO: move to constants
        private static string attributeNameAnnotation = "[AttributeName]";
        private static string attributeTypeAnnotation = "[AttributeType]";
        private static string attributesBlockAnnotation = "[attribs]";

        public static readonly string ClassName = "Environment.cs";

        public static string GenerateSource(string namespaceName, List<XMLAttribute> attributes)
        {
            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, EnvironmentTemplateFilePath);
            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);

            if (attributes != null)
            {
                var attributesSb = new StringBuilder();
                if (!(attributes.Count == 1 && attributes[0].Name == null))
                {
                    foreach (var element in attributes)
                    {
                        IoHelper.GetFileTextToStringBuilder(attributesSb, EnvironmentAttributeTemplateFilePath);
                        attributesSb.Replace(attributeNameAnnotation, element.Type);
                        attributesSb.Replace(attributeTypeAnnotation, element.Name);
                        attributesSb.Append("\n");
                    }
                }
                mainSb.Replace(attributesBlockAnnotation, attributesSb.ToString());
            }
  
            return mainSb.ToString();
        }
    }
}