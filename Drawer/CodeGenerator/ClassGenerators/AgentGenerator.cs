﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public class AgentGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "AgentTemplate.tmpl";
        private static readonly string SensorTemplateFile = Paths.TemplatesPath + "SensorTemplate.tmpl";
        private static readonly string ProtocolTemplateFile = Paths.TemplatesPath + "AgentProtocoleMethodTemplate.tmpl";
        public static List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, XmlParser xmlParser)
        {
            var sources = new List<string>();

            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, TemplateFile);
            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
            mainSb.Replace(BasicAnnotations.ClassName, namespaceName);

            foreach (var xmlAgentRole in xmlParser.AgentRoles)
            {
                var agentSb = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(agentSb, TemplateFile);
                agentSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                agentSb.Replace(BasicAnnotations.ClassName, xmlAgentRole.Name + "Agent");
                agentSb.Replace("[StateName]", xmlAgentRole.TargetState);
                agentSb.Replace("[BeliefsName]", xmlAgentRole.TargetBelief);
                agentSb.Replace("[AgentRole]", xmlAgentRole.Name);

                var role = xmlAgentRole;
                var agentsWithRole = xmlParser.AgentInstances.Where(agent => agent.Role == role.Name);
                var agentInstanceSb = agentSb;
                foreach (var xmlAgentInstance in agentsWithRole)
                {
  
                    var sensorSb = new StringBuilder();
                    var sensors = xmlAgentInstance.Sensors;
                    foreach (var xmlSensor in sensors)
                    {
                        IoHelper.GetFileTextToStringBuilder(sensorSb, SensorTemplateFile);
                        sensorSb.Replace("[SensorName]", xmlSensor.Name);
                        string sensorScriptSource = GetSensorScriptSource(xmlParser.SourceFolder + xmlSensor.LogicStcriptPath);
                        sensorSb.Replace("[SensorScript]", sensorScriptSource);
                    }
                    agentInstanceSb.Replace("[Sensors]", sensorSb.ToString());

                    //protocols
                    var protocolsSB = new StringBuilder();
                    var protocolConnectionScriptSB = new StringBuilder();
                    var otherMethodsSB = new StringBuilder();
                    foreach (var protocol in xmlAgentRole.Protocols)
                    {
                        var protocolsDefinitions = xmlParser.Protocols.Where(prot => prot.Name == protocol);
                        foreach (var protocolsDefinition in protocolsDefinitions)
                        {
                            foreach (var xmlProtocolFunc in protocolsDefinition.Funcs)
                            {
                                var paramSB = new StringBuilder();
                                for (var i = 0; i < xmlProtocolFunc.ParameterTypes.Count - 1; i++)
                                {
                                    paramSB.Append(xmlProtocolFunc.ParameterTypes[i] + " param" + i);
                                }

                                var source = xmlProtocolFunc.Source;
                                var sourceCode = new StringBuilder();
                                IoHelper.GetFileTextToStringBuilder(sourceCode, xmlParser.SourceFolder + source);

                                var dumbMethodBody = "";
                                if (sourceCode.ToString() == "")
                                    switch (xmlProtocolFunc.ParameterTypes.Last())
                                    {
                                        case "bool":
                                            dumbMethodBody = "return true;";
                                            break;
                                        case "string":
                                            dumbMethodBody = @"return """";";
                                            break;
                                        case "int":
                                            dumbMethodBody = "return 0;";
                                            break;
                                        case "void":
                                            dumbMethodBody = "return ;";
                                            break;
                                    }
                                else
                                {
                                    dumbMethodBody = sourceCode.ToString();
                                }
                                otherMethodsSB.Append("private " +  xmlProtocolFunc.ParameterTypes.Last() + " "
                                    + xmlProtocolFunc.Name + "(" + paramSB + ") \n\t\t{\n\t\t" + 
                                    dumbMethodBody + "\n\t\t}\n\n\t\t");
                                protocolConnectionScriptSB.Append("clientProtocol." + xmlProtocolFunc.Name + " = " + xmlProtocolFunc.Name 
                                    + ";\n\t\t\t");
                            }
                        }
                        IoHelper.GetFileTextToStringBuilder(protocolsSB, ProtocolTemplateFile);
                        protocolsSB.Replace("[ProtocolName]", protocol);
                        protocolsSB.Replace("[Script]", protocolConnectionScriptSB.ToString());
                    }
                    agentInstanceSb.Replace("[ProtocolMethods]", protocolsSB.ToString());
                    agentInstanceSb.Replace("[OtherMethods]", otherMethodsSB.ToString());
                }
                ClassNames.Add(xmlAgentRole.Name);
                sources.Add(agentSb.ToString());
            }

            //return mainSb.ToString();
            return sources;
        }

        private static string GetSensorScriptSource(string scriptPath)
        {
            var sourceSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(sourceSb, scriptPath);
            return sourceSb.ToString();
        }
    }
}