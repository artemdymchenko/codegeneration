﻿using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public class BaseAgentClassGenerator
    {
        private static readonly string AgentInterfaceTemplateFilePath = Paths.TemplatesPath + "BaseAgentTemplate.tmlp";

        public static readonly string ClassName = "BaseAgent.cs";

        public static string GenerateSource(string namespaceName)
        {
            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, AgentInterfaceTemplateFilePath);

            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);

            return mainSb.ToString();
        }                                                                                                                                                                                                                                                                                                                                        
    }
}