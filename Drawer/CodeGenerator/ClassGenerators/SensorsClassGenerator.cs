﻿using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;

namespace CodeGenerator.ClassGenerators
{
    public class SensorsClassGenerator
    {
        private static readonly string SensorTemplateFilePath = Paths.TemplatesPath + "SensorTemplate.tmlp";
        public static readonly string ClassName = "DumbSensor.cs";

        public static string GenerateSource(string namespaceName)
        {
            var mainSb = new StringBuilder();
            IoHelper.GetFileTextToStringBuilder(mainSb, SensorTemplateFilePath);

            mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);

            return mainSb.ToString();
        }
    }
}