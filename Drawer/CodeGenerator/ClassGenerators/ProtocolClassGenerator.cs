﻿using System.Collections.Generic;
using System.Text;
using CodeGenerator.ClassGeneratorsHelpers;
using CodeGenerator.XMLEntities;

namespace CodeGenerator.ClassGenerators
{
    public class ProtocolClassGenerator
    {
        private static readonly string TemplateFile = Paths.TemplatesPath + "ProtocolTemplate.tmpl";
        private static readonly string TemplateFuncFile = Paths.TemplatesPath + "ProtocolFuncTemplate.tmpl";
        public static List<string> ClassNames = new List<string>();

        public static List<string> GenerateSources(string namespaceName, List<XMLProtocol> protocols)
        {
            var sources = new List<string>();

            foreach (var xmlProtocol in protocols)
            {
                var mainSb = new StringBuilder();
                IoHelper.GetFileTextToStringBuilder(mainSb, TemplateFile);
                mainSb.Replace(BasicAnnotations.NamespaceNameAnnotation, namespaceName);
                mainSb.Replace(BasicAnnotations.ClassName, xmlProtocol.Name);

                var funcSb = new StringBuilder();
                foreach (var xmlProtocolFunc in xmlProtocol.Funcs)
                {
                    IoHelper.GetFileTextToStringBuilder(funcSb, TemplateFuncFile);
                    var funcTypesSb = new StringBuilder();
                    foreach (var funcType in xmlProtocolFunc.ParameterTypes)
                    {
                        if (funcTypesSb.Length != 0)
                            funcTypesSb.Append(",");
                        funcTypesSb.Append(funcType);
                    }
                    funcSb.Replace(BasicAnnotations.ProtocolFuncTypes, funcTypesSb.ToString());
                    funcSb.Replace(BasicAnnotations.ProtocolMethodName, xmlProtocolFunc.Name);
                    funcSb.Append("\n\t\t");
                }
                mainSb.Replace(BasicAnnotations.FuncsRegionAnnotation, funcSb.ToString());

                ClassNames.Add(xmlProtocol.Name);
                sources.Add(mainSb.ToString());
            }

            return sources;
        } 
    }
}