﻿namespace CodeGenerator.ClassGenerators
{
    public static class BasicAnnotations
    {
        private static string _namespaceNameAnnotation = "[$$]";
        private static string _className = "[ClassName]";

        //Protocol annotations  
        private static string _funcsRegionAnnotation = "[funcs]";
        private static string _protocolFuncTypes = "[ProtocolFuncTypes]";
        private static string _protocolMethodName = "[ProtocolMethodName]";

        public static string NamespaceNameAnnotation
        {
            get { return _namespaceNameAnnotation; }
        }

        public static string FuncsRegionAnnotation
        {
            get { return _funcsRegionAnnotation; }
        }

        public static string ProtocolFuncTypes
        {
            get { return _protocolFuncTypes; }
        }

        public static string ProtocolMethodName
        {
            get { return _protocolMethodName; }
        }

        public static string ClassName
        {
            get { return _className; }
        }
    }
}