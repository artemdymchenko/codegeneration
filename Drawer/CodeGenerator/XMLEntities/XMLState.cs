﻿using System.Collections.Generic;

namespace CodeGenerator.XMLEntities
{
    public class XMLState
    {
        public XMLState(string name)
        {
            Name = name;
            Attributes = new List<XMLAttribute>();
            Protocols = new List<string>();
        }

        public string Name { get; set; }
        public List<XMLAttribute> Attributes { get; set; }
        public List<string> Protocols { get; set; }
    }
}