﻿namespace CodeGenerator.XMLEntities
{
    public class XMLGoal
    {
        public XMLGoal(string name, string targetState)
        {
            Name = name;
            TargetState = targetState;
        }

        public string Name { get; private set; }
        public string TargetState { get; private set; }
    }
}