﻿using System.Collections.Generic;

namespace CodeGenerator.XMLEntities
{
    public class XMLNamedAttributeList
    {
        public XMLNamedAttributeList(string name)
        {
            Name = name;
            Attributes = new List<XMLAttribute>();
        }

        public string Name { get; set; }
        public List<XMLAttribute> Attributes { get; set; }
    }
}