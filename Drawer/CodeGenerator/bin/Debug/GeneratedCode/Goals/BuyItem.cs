using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
    public class BuyItem : IGoal<BuyerState>
    {
        public bool IsAchieved(BuyerState state)
        {
            return true;
        }

        public List<IPlan> GetPlansForAchievingTheGoal(BuyerState state)
        {
            var plans = new List<IPlan>
            {
                new FastBuyItemPlan(this, state),new SmartBuyItemPlan(this, state),
            };

            return plans;
        }
    }
}
