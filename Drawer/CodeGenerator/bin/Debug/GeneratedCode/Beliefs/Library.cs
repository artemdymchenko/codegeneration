using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class Library
    {
		public List<string> Titles { get; set; }
		public List<int> TimesBought { get; set; }
		

		public Library()
		{
			Titles = new List<string>();
			TimesBought = new List<int>();
			
		}
	}
}
