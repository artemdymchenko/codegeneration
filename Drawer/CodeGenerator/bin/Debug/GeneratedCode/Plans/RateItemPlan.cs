using System;
using System.Linq;
using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class RateItemPlan : IPlan
    {
		private readonly RateItem _goal;
        private readonly BuyerState _state;

		public RateItemPlan(RateItem goal, BuyerState state)
        {
            _goal = goal;
            _state = state;
        }

		public void ExecutePlan()
        {
			//************
        }

        public float GetUtility()
        {
			return 10;
        }
	}
}
