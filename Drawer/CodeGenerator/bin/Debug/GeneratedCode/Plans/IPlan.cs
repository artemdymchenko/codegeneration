//dumb protocol template
namespace GeneratedArchitecture.GeneratedCode
{
    public interface IPlan
    {
        void ExecutePlan();
        float GetUtility();
    }
}
