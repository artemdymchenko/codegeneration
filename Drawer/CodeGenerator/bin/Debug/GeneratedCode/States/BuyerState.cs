using System.Collections.Generic;
using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class BuyerState
    {
		public Args string { get; set; }
		public UserInputCommand string { get; set; }
		public Titles List!string { get; set; }
		public Stores List!string { get; set; }
		public Ratings List!string { get; set; }
		public SmartSearch bool { get; set; }
		
		public BuyerSellerProtocol BuyerSellerProtocol { get; private set; }
		public BuyerBuyerProtocol BuyerBuyerProtocol { get; private set; }
		
		public BuyerState()
		{
			string = new Args();
			string = new UserInputCommand();
			List!string = new Titles();
			List!string = new Stores();
			List!string = new Ratings();
			bool = new SmartSearch();
			BuyerSellerProtocol = new BuyerSellerProtocol();
		BuyerBuyerProtocol = new BuyerBuyerProtocol();
		
		}
	}
}
