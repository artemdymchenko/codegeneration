using System;

namespace GeneratedArchitecture.GeneratedCode
{
	public class BuyerSellerProtocol
    {
		public Func<string,bool> HasItemTitle;
		public Func<string> GetStoreInfo;
		public Func<string,bool> BuyItem;
		
	}
}
