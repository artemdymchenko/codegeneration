using System;
using System.ComponentModel;

namespace GeneratedArchitecture.GeneratedCode
{
    public class BaseSensor
    {
		public string AttributeName { get; private set; }

        public BaseSensor(string attributeNameName)
        {
            Activation = null;
            AttributeName = attributeNameName;
            Environment.Instance.PropertyChanged += ActivateSensor;
        }

        public BaseSensor(string attributeNameName, Action<object, PropertyChangedEventArgs> action)
        {
            Activation = action;
            AttributeName = attributeNameName;
            Environment.Instance.PropertyChanged += ActivateSensor;
        }

        private void ActivateSensor(object sender, PropertyChangedEventArgs e)
        {
            if (AttributeName != e.PropertyName)
                return;

            if (Activation != null) Activation(sender, e);
        }

        public Action<object, PropertyChangedEventArgs> Activation;
	}
};
