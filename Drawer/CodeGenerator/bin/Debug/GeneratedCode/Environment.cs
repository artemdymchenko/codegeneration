using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GeneratedArchitecture.GeneratedCode
{
    public class Environment : INotifyPropertyChanged
    {
        //Environment is a Singletone
        private Environment()
        {
        }

        private static Environment _instance;
        public static Environment Instance
        {
            get { return _instance ?? (_instance = new Environment()); }
        }

        //property changed notificator
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
		
		//attributes of the environment
		private string _ItemNamesOfStore1;
        public string ItemNamesOfStore1
        {
            get { return _ItemNamesOfStore1; }
            set
            {
                _ItemNamesOfStore1 = value;
                OnPropertyChanged();
            }
        }
		private string _ItemNamesOfStore2;
        public string ItemNamesOfStore2
        {
            get { return _ItemNamesOfStore2; }
            set
            {
                _ItemNamesOfStore2 = value;
                OnPropertyChanged();
            }
        }
		private string _ItemNamesOfStore3;
        public string ItemNamesOfStore3
        {
            get { return _ItemNamesOfStore3; }
            set
            {
                _ItemNamesOfStore3 = value;
                OnPropertyChanged();
            }
        }
		private string _UserInput;
        public string UserInput
        {
            get { return _UserInput; }
            set
            {
                _UserInput = value;
                OnPropertyChanged();
            }
        }
		private string _UserOutput;
        public string UserOutput
        {
            get { return _UserOutput; }
            set
            {
                _UserOutput = value;
                OnPropertyChanged();
            }
        }

    }
};

