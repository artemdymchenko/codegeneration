using System.Collections.Generic;
using System.Linq;
using System;

namespace .GeneratedCode
{
    public class BlackBoard
    {
        private readonly List<BaseAgent> _agents;

        public void RegisterSimpleAgent(string role, BaseAgent newAgent)
        {
		    Console.WriteLine("An agent with role " + role + " was registered in the black board");
            _agents.Add(newAgent);
        }

        //is a singleton
        private BlackBoard()
        {
            _agents = new List<BaseAgent>();
        }

        private static BlackBoard _instance;
        public static BlackBoard Instance
        {
            get { return _instance ?? (_instance = new BlackBoard()); }
        }

        public List<BaseAgent> GetAllAgentsByRole(string role)
        {
            return _agents.Where(baseAgent => baseAgent.Role == role).ToList();
        }

        public BaseAgent GetAgentByName(string name)
        {
            return _agents.Where(baseAgent => baseAgent.Name == name).ToList().FirstOrDefault();
        }
    }
};

