using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace .GeneratedCode
{
    public class Environment : INotifyPropertyChanged
    {
        //Environment is a Singletone
        private Environment()
        {
        }

        private static Environment _instance;
        public static Environment Instance
        {
            get { return _instance ?? (_instance = new Environment()); }
        }

        //property changed notificator
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
		
		//attributes of the environment

    }
};

