using System.Collections.Generic;

namespace .GeneratedCode
{
    public interface IGoal<in TAgentState>
    {
        bool IsAchieved(TAgentState state);

        List<IPlan> GetPlansForAchievingTheGoal(TAgentState state);
    }
}
