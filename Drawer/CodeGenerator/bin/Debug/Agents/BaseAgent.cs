using System;
using System.Collections.Generic;

namespace .GeneratedCode
{
    public class BaseAgent
    {
        public Guid AgentGuid { get; private set; }
        public string Role { get; private set; }
        public string Name { get; private set; }
        protected BaseAgent(string role, string name)
        {
            Role = role;
            Name = name;
            BlackBoard.Instance.RegisterSimpleAgent(role, this);
            AgentGuid = Guid.NewGuid();
        }
    }

	public abstract class BaseStateBeliefAgent<TState, TBelief> : BaseAgent where TBelief : new() where TState : new()
    {
        public List<BaseSensor> Sensors;
        public TBelief Beliefs;
        public TState State;
        public List<IGoal<TState>> Goals;

        protected BaseStateBeliefAgent(string role, string name) : base(role, name)
        {
            Sensors = new List<BaseSensor>();
            Beliefs = new TBelief();
            State = new TState();
            Goals = new List<IGoal<TState>>();
        }

        public void ExecuteTheBestPlan()
        {
            Console.WriteLine("A plan picking procedure form " + Name + " has started");
            if (Goals.Count == 0)
                return;
            Console.WriteLine("An agent has goals");
            //search for a plan with maximum utility
            IPlan bestPlan = null;
            IGoal<TState> bestGoal = null;
            foreach (var goal in Goals)
            {
                foreach (var plan in goal.GetPlansForAchievingTheGoal(State))
                {
                    if (bestPlan == null || bestPlan.GetUtility() < plan.GetUtility())
                    {
                        bestPlan = plan;
                        bestGoal = goal;
                    }
                }
            }

            //execute the best plan
            if (bestPlan != null)
            {
                Console.WriteLine("An agent determined the best plan");
                bestPlan.ExecutePlan();
            }

            //pop this goal
            if (bestGoal != null && bestGoal.IsAchieved(State))
            {
                Goals.Remove(bestGoal);
            }
        }
    }
};
