﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Automation.Peers;
using CodeGenerator;
using Drawer.Application;
using Drawer.Application.Core;
using Drawer.CustomTypes.VertexTypes;
using Drawer.Data;
using Drawer.DependencyInjection;
using Drawer.Presentation.Dialogs;
using Drawer.Views;
using Drawer.XMLGenerator;
using Microsoft.Win32;
using QuickGraph;

namespace Drawer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            Resolver.Instance.DataInstance.PropertyChanged += UpdateGraph;
            UpdateGraph(null, null);
            InitializeComponent();
        }

        private void UpdateGraph(object sender, EventArgs e)
        {
            Update();
        }

        private void Update()
        {
            DataContext = Resolver.Instance.GraphGeneratorService
                .GenerateGraphToVisualize(Resolver.Instance.DataInstance);
        }

        private void ContextMenuClick(object sender, RoutedEventArgs e)
        {
            Update();
        }

        private void GenerateCode_click(object sender, RoutedEventArgs e)
        {
            Resolver.Instance.CommandService.GenerateXmlCommand();
            var codeGenerator = new CodeGeneratorClass();
            codeGenerator.GenerateSourceCode("input.xml", false);
        }

        private void GenerateDLL_Click(object sender, RoutedEventArgs e)
        {
            Resolver.Instance.CommandService.GenerateXmlCommand();
            var codeGenerator = new CodeGeneratorClass();
            codeGenerator.GenerateSourceCode("input.xml", true);
        }

        private void OpenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dlg = Resolver.Instance.DefautDialogService.OpenFileDialog();
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                Resolver.Instance.CommandService.LoadDocumentCommand(dlg.FileName);
            }
        }

        private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dlg = Resolver.Instance.DefautDialogService.SaveFileDialog();
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                Resolver.Instance.CommandService.SaveDocumentCommand(dlg.FileName);
            }
        }

        private void NewMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Resolver.Instance.DataInstance.BlockUpdate = true;
            Resolver.Instance.CommandService.ClearDataCommand();
            Resolver.Instance.DataInstance.BlockUpdate = false;
            Update();
        }

        private void GenerateXML_Click(object sender, RoutedEventArgs e)
        {
            Resolver.Instance.CommandService.GenerateXmlCommand();
        }

        private void LoadScript_Click(object sender, RoutedEventArgs e)
        {
            Resolver.Instance.DataInstance.BlockUpdate = true;
            Resolver.Instance.CommandService.LoadScript();
            Resolver.Instance.DataInstance.BlockUpdate = false;
            Update();
        }

 
    }
}
