using System.Collections.Generic;
using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class BuyerState
    {
		public string Args;
		public string UserInputCommand;
		public List<string> Titles;
		public List<string> Stores;
		public List<string> Ratings;
		public bool SmartSearch;
		
		public BuyerSellerProtocol BuyerSellerProtocol { get; private set; }
		public BuyerBuyerProtocol BuyerBuyerProtocol { get; private set; }
		
		public BuyerState()
		{
			Args = "";
			UserInputCommand = "";
			Titles = new List<string>();
			Stores = new List<string>();
			Ratings = new List<string>();
			SmartSearch = false;
			BuyerSellerProtocol = new BuyerSellerProtocol();
		BuyerBuyerProtocol = new BuyerBuyerProtocol();
		
		}
	}
}
