using System;
using System.Linq;
using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class AskStoresAboutItems : IPlan
    {
		private readonly FindItem _goal;
        private readonly BuyerState _state;

		public AskStoresAboutItems(FindItem goal, BuyerState state)
        {
            _goal = goal;
            _state = state;
        }

		public void ExecutePlan()
        {
			//************
        }

        public float GetUtility()
        {
			return 10;
        }
	}
}
