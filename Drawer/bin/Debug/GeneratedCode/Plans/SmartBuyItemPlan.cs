using System;
using System.Linq;
using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
	public class SmartBuyItemPlan : IPlan
    {
		private readonly BuyItem _goal;
        private readonly BuyerState _state;

		public SmartBuyItemPlan(BuyItem goal, BuyerState state)
        {
            _goal = goal;
            _state = state;
        }

		public void ExecutePlan()
        {
			//************
        }

        public float GetUtility()
        {
			return 10;
        }
	}
}
