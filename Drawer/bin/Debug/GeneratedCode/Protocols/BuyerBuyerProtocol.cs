using System;

namespace GeneratedArchitecture.GeneratedCode
{
	public class BuyerBuyerProtocol
    {
		public Func<string,int> GetRatingForItemInStore;
		public Func<string,bool> BuyerBoughtTheItem;
		public Func<string,string> GetBestStoreForItem;
		
	}
}
