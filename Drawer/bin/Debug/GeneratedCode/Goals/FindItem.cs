using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
    public class FindItem : IGoal<BuyerState>
    {
        public bool IsAchieved(BuyerState state)
        {
            return true;
        }

        public List<IPlan> GetPlansForAchievingTheGoal(BuyerState state)
        {
            var plans = new List<IPlan>
            {
                new AskStoresAboutItems(this, state),
            };

            return plans;
        }
    }
}
