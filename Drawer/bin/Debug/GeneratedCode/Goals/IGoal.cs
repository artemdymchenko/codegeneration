using System.Collections.Generic;

namespace GeneratedArchitecture.GeneratedCode
{
    public interface IGoal<in TAgentState>
    {
        bool IsAchieved(TAgentState state);

        List<IPlan> GetPlansForAchievingTheGoal(TAgentState state);
    }
}
