using System.ComponentModel;

namespace GeneratedArchitecture.GeneratedCode
{
    public class BuyerAgent : BaseStateBeliefAgent<BuyerState, ReaderExperience>
    {
        public BuyerAgent(string name) : base("Buyer", name)
        {
        }

		public void UserInputChangeCallback1(object o, PropertyChangedEventArgs e)
        {
			var attrib = (o.GetType().GetProperty(e.PropertyName).GetValue(Environment.Instance, null)).ToString();
			
        }
		public void EstablishConnection(BuyerBuyerProtocol clientProtocol)
        {
			clientProtocol.GetRatingForItemInStore = GetRatingForItemInStore;
			clientProtocol.BuyerBoughtTheItem = BuyerBoughtTheItem;
			clientProtocol.GetBestStoreForItem = GetBestStoreForItem;
			
        }
		private int GetRatingForItemInStore(string param0) 
		{
		return 0;
		}

		private bool BuyerBoughtTheItem(string param0) 
		{
		return true;
		}

		private string GetBestStoreForItem(string param0) 
		{
		return "";
		}

		
    }
}
