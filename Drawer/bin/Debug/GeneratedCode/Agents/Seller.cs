using System.ComponentModel;

namespace GeneratedArchitecture.GeneratedCode
{
    public class SellerAgent : BaseStateBeliefAgent<SellerState, Library>
    {
        public SellerAgent(string name) : base("Seller", name)
        {
        }

		public void StoreChange1(object o, PropertyChangedEventArgs e)
        {
			var attrib = (o.GetType().GetProperty(e.PropertyName).GetValue(Environment.Instance, null)).ToString();
			
        }
		public void EstablishConnection(BuyerSellerProtocol clientProtocol)
        {
			clientProtocol.HasItemTitle = HasItemTitle;
			clientProtocol.GetStoreInfo = GetStoreInfo;
			clientProtocol.BuyItem = BuyItem;
			
        }
		private bool HasItemTitle(string param0) 
		{
		return true;
		}

		private string GetStoreInfo() 
		{
		return "";
		}

		private bool BuyItem(string param0) 
		{
		return true;
		}

		
    }
}
