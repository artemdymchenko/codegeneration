﻿using Drawer.Application;
using Drawer.Application.AttributeService;
using Drawer.Application.Core;

using Drawer.Presentation.Dialogs;
using Drawer.XMLGenerator;
using Drawer.Data;
using Drawer.Application.CommandsService;

namespace Drawer.DependencyInjection
{
    public class Resolver : IResolver
    {
        private static Resolver _instance;
        public static Resolver Instance
        {
            get
            {
                return _instance ?? (_instance = new Resolver());
            }
        }

        public IDefautDialogService DefautDialogService { get; set; }
        public ICommandService CommandService { get; set; }
        public DataHolder DataInstance { get; set; }
        public IGraphGeneratorService GraphGeneratorService { get; set; }
        public IXMLGenerator XMLGenerator { get; set; }
        public BaseParser ConsoleCommandParser { get; set; }
        public IAttributeValidationService AttributeValidationService { get; set; }

        private Resolver()
        {
            DefautDialogService = new DefaultDialogsService();
            DataInstance = new DataHolder();
            GraphGeneratorService = new GraphGeneratorService();
            XMLGenerator = new XmlGenerator();
            ConsoleCommandParser = new ConsoleCommandParser();
            AttributeValidationService = new AttributeValidationValidationService();

            CommandService = new CommandService(DataInstance, XMLGenerator, ConsoleCommandParser);
        }
    }
}