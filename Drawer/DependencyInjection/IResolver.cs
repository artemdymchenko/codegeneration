﻿using Drawer.Application;
using Drawer.Application.AttributeService;
using Drawer.Application.CommandsService;
using Drawer.Application.Core;

using Drawer.Presentation.Dialogs;
using Drawer.XMLGenerator;
using Drawer.Data;

namespace Drawer.DependencyInjection
{
    public interface IResolver
    {
        IDefautDialogService DefautDialogService { get; set; }
        ICommandService CommandService { get; set; }
        DataHolder DataInstance { get; set; }
        IGraphGeneratorService GraphGeneratorService { get; set; }
        IXMLGenerator XMLGenerator { get; set; }
        BaseParser ConsoleCommandParser { get; set; }
        IAttributeValidationService AttributeValidationService { get; set; }
    }
}