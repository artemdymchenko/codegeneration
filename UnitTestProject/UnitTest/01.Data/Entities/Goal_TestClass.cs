﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class Goal_TestClass : BaseEntity_TestClass
    {
        private Goal _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new Goal("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Goal_Creation_NoPropertyChangedEventsHappen()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void Goal_PropertyChangedCheck()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
            
            _testedEntity.AssociatedStateName = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 1);
           
            _testedEntity.Name = "Test2";
            Assert.AreEqual(_receivedEvents.Count, 2);
            
            _testedEntity.Script = "Script";
            Assert.AreEqual(_receivedEvents.Count, 3);
        }

        [Test]
        public void ConnectToPlan_Check()
        {
            var plan = new Plan("Test");
            _testedEntity.ConnectToPlan(plan);

            Assert.AreEqual(_testedEntity.GetPlans().Contains(plan), true);
            Assert.AreEqual(plan.Goal.Guid, _testedEntity.Guid);
        }

        [Test]
        public void ConnectToPlan_PropertyChangedCheck()
        {
            var plan = new Plan("Test");

            _testedEntity.ConnectToPlan(plan);

            Assert.AreEqual(_receivedEvents.Count, 1);
        }

        [Test]
        public void DisconnectFromPlan_Existing_Check()
        {
            var plan = new Plan("Test");

            _testedEntity.ConnectToPlan(plan);
            _testedEntity.DisonnectFromPlan(plan);

            Assert.AreEqual(_testedEntity.GetPlans().Contains(plan), false);
        }

        [Test]
        public void DisconnectFromPlan_NonExisting_Check()
        {
            var plan = new Plan("Test");

            _testedEntity.DisonnectFromPlan(plan);

            Assert.AreEqual(_testedEntity.GetPlans().Contains(plan), false);
        }

        [Test]
        public void DisconnectFromPlan_PropertyChangedCheck()
        {
            var plan = new Plan("Test");

            _testedEntity.ConnectToPlan(plan);
            _testedEntity.DisonnectFromPlan(plan);

            Assert.AreEqual(_receivedEvents.Count, 2);
        }
    }
}