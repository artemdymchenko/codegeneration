﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class Protocol_TestClass : BaseTypeName_TestClsass
    {
        private Protocol _testedEntity;

        [SetUp]
        public new void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new Protocol("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Creation_EmptyGuids_Check()
        {
            Assert.AreEqual(_testedEntity.Client, Guid.Empty);
            Assert.AreEqual(_testedEntity.Server, Guid.Empty);
        }

        [Test]
        public void Creation_NoPropertyChanged_Check()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void PropertyChangedIsCalled_Check()
        {
            _testedEntity.Name  = "name";
            Assert.AreEqual(_receivedEvents.Count, 1);
            _testedEntity.Client = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 2);
            _testedEntity.Server = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 3);
        }
    }
}