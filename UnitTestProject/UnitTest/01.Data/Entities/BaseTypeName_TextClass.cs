﻿using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class BaseTypeName_TestClsass : BaseEntity_TestClass
    {
        private BaseTypeNameDictionary _testedEntity;

        [SetUp]
        public new void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new BaseTypeNameDictionary();
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void BaseTypeName_Creation_NoPropertyChangedEventsHappen()
        {
            //act
            var v = new BaseTypeNameDictionary();

            //Assert
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void Add_PropertyChangedCheck()
        {
            //act
            _testedEntity.Add("testType", "testValue");

            //Assert
            Assert.Greater(_receivedEvents.Count, 0);
            Assert.AreEqual(_testedEntity.Attribs.Count, 1);
        }

        [TestCase("t", "n", "n", true )]
        [TestCase("t", "n", "r", false )]
        [TestCase("t", "n", "t", false )]
        public void ContainsKey_Check(string newType, string newName, string searchedName, bool containsKey)
        {
            //act
            _testedEntity.Add(newType, newName);

            //Assert
            Assert.AreEqual(_testedEntity.ContainsKey(searchedName), containsKey);
        }

        [TestCase("t", "n", "n", true)]
        [TestCase("t", "n", "t", false)]
        [TestCase("t", "n", "r", false)]
        public void Remove_Existing_Check(string newType, string newName, string nameToRemove, bool succeed)
        {
            //act
            _testedEntity.Add(newType, newName);
            bool removed = _testedEntity.Remove(nameToRemove);
            //Assert
            Assert.AreEqual(removed, succeed);

            if (removed)
                Assert.AreEqual(_testedEntity.Attribs.Count, 0);
        }

        [Test]
        public void Clear_OnCalled_PropertyChangedCheck()
        {
            _testedEntity.Clear();

            Assert.AreEqual(_receivedEvents.Count, 1);
        }

        [TestCase("Key1", "Value1", "Key2", "Value2", false)]
        [TestCase("Key1", "Value1", "Key1", "Value2", false)]
        [TestCase("Key1", "Value1", "Key2", "Value1", false)]
        [TestCase("Key1", "Value1", "Key1", "Value1", true)]
        public void Equals_OneObjectAddedToEachEntity_Test(
            string key1, string value1, string key2, string value2, bool expectedResult)
        {
            var newEntity = new BaseTypeNameDictionary();
            _testedEntity.Add(key1, value1);
            newEntity.Add(key2, value2);

            Assert.AreEqual(expectedResult, _testedEntity.Equals(newEntity));
        }

        [Test]
        public void Equals_ObjectAddedToOneEntity_Test()
        {
            var newEntity = new BaseTypeNameDictionary();
            _testedEntity.Add("key1", "value1");
            _testedEntity.Add("key2", "value2");
            newEntity.Add("key1", "value1");

            Assert.IsFalse(_testedEntity.Equals(newEntity));
        }
    }
}