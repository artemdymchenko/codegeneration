﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class AgentRole_TestClass : BaseEntity_TestClass
    {
        private List<string> _receivedEvents;
        private AgentRole _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new AgentRole("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Agent_Creation_NoPropertyChangedEventsHappen()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }
    }
}