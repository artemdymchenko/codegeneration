﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class StateTemplate_TestClass : BaseTypeName_TestClsass
    {
        private StateTemplate _testedEntity;

        [SetUp]
        public new void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new StateTemplate("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Creation_EmptyAssociatedAgentGuid_Check()
        {
            Assert.AreEqual(_testedEntity.AssociatedAgentRoleGuid, Guid.Empty);
        }

        [Test]
        public void Creation_NoPropertyChanged_Check()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void PropertyChangedIsCalled_Check()
        {
            _testedEntity.Name  = "name";
            Assert.AreEqual(_receivedEvents.Count, 1);
            _testedEntity.AssociatedAgentRoleGuid = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 2);
        }
    }
}