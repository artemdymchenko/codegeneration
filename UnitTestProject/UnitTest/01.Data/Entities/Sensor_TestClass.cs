﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class Sensor_TestClass : BaseEntity_TestClass
    {
        private Sensor _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new Sensor("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Sensor_Creation_NoPropertyChangedEventsHappen()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void Sensor_PropertyChangedCheck()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
            
            _testedEntity.AssociatedAgentInstanceGuid = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 1);
           
            _testedEntity.Name = "Test2";
            Assert.AreEqual(_receivedEvents.Count, 2);

            _testedEntity.Script = "Test2";
            Assert.AreEqual(_receivedEvents.Count, 3);

            _testedEntity.TargetEnvironment = "Test2";
            Assert.AreEqual(_receivedEvents.Count, 4);
        }
    }
}