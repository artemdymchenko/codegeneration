﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class Plan_TestClass : BaseEntity_TestClass
    {
        private Plan _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new Plan("Test");
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Plan_Creation_NoPropertyChangedEventsHappen()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }

        [Test]
        public void Plan_PropertyChangedCheck()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
            
            _testedEntity.AssociatedAgentGuid = Guid.NewGuid();
            Assert.AreEqual(_receivedEvents.Count, 1);
           
            _testedEntity.Name = "Test2";
            Assert.AreEqual(_receivedEvents.Count, 2);
        }

        [Test]
        public void ConnectToGoal_Check()
        {
            var goal = new Goal("Test");
            _testedEntity.ConnectToAGoal(goal);

            Assert.AreEqual(_testedEntity.Goal == goal, true);
            Assert.AreEqual(goal.GetPlans().Contains(_testedEntity), true);
        }

        [Test]
        public void ConnectToGoal_PropertyChangedCheck()
        {
            var goal = new Goal("Test");

            _testedEntity.ConnectToAGoal(goal);

            Assert.AreEqual(_receivedEvents.Count, 1);
        }

        [Test]
        public void DisconnectFromGoal_Existing_Check()
        {
            var goal = new Goal("Test");

            _testedEntity.ConnectToAGoal(goal);
            _testedEntity.DisconnectFromGoal(goal);

            Assert.AreEqual(_testedEntity.Goal == goal, false);
        }

        [Test]
        public void DisconnectFromGoal_NonExisting_Check()
        {
            var goal = new Goal("Test");

            _testedEntity.DisconnectFromGoal(goal);

            Assert.AreEqual(_testedEntity.Goal == goal, false);
        }

        [Test]
        public void DisconnectFromGoal_PropertyChangedCheck()
        {
            var goal = new Goal("Test");

            _testedEntity.ConnectToAGoal(goal);
            _testedEntity.DisconnectFromGoal(goal);

            Assert.AreEqual(_receivedEvents.Count, 2);
        }
    }
}