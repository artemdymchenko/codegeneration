﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Drawer.Data.Entities;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTestProject.Data.Entities
{
    [TestFixture]
    public class BaseEntity_TestClass
    {
        protected List<string> _receivedEvents;
        private BaseEntity _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new BaseEntity();
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void BaseEntity_Creation_GuidIsInitialized()
        {
            Assert.AreNotEqual(_testedEntity.Guid, new Guid());
        }

        [Test]
        public void BaseTypeName_Creation_NoPropertyChangedEventsHappen()
        {
            Assert.AreEqual(_receivedEvents.Count, 0);
        }
    }
}