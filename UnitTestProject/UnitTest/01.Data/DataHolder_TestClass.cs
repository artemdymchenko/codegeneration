﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Navigation;
using Drawer.Data;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.Data
{
    [TestFixture]
    public class Data_TestClass
    {
        private List<string> _receivedEvents;
        private DataHolder _testedEntity;

        [SetUp]
        public void Init()
        {
            _receivedEvents = new List<string>();
            _testedEntity = new DataHolder();
            _testedEntity.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                _receivedEvents.Add(e.PropertyName);
            };
        }

        [Test]
        public void Instance_Get_NotNull()
        {
            Assert.AreNotEqual(_testedEntity, null);
        }

        [Test]
        public void Environment_PropertyChanged_IsNotified()
        {
            _testedEntity.Environment.Add("asd","asd");

            Assert.AreEqual(_receivedEvents.Count, 1);

            _testedEntity.Environment.Remove("asd");

            Assert.AreEqual(_receivedEvents.Count, 2);
        }

        private void CheckRemove<T>(ObservableCollection<T> collection, T newItem)
        {
            _receivedEvents.Clear();
            collection.Remove(newItem);
            Assert.AreEqual(_receivedEvents.Count, 1);
        }

        private void CheckAdd<T>(ObservableCollection<T> collection, T newItem)
        {
            _receivedEvents.Clear();
            collection.Add(newItem);
            Assert.AreEqual(_receivedEvents.Count, 1);
        }

        [Test]
        public void BeliefsTemplates_PropertyChanged_IsNotified()
        {
            var newItem = new BeliefsTemplate("Test");

            CheckAdd(_testedEntity.BeliefsTemplates, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.AssociatedAgentRoleGuid = Guid.NewGuid();
            Assert.AreEqual(2, _receivedEvents.Count);

            CheckRemove(_testedEntity.BeliefsTemplates, newItem);
        }

        [Test]
        public void StateTemplates_PropertyChanged_IsNotified()
        {
            var newItem = new StateTemplate("Test");

            CheckAdd(_testedEntity.StateTemplates, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.AssociatedAgentRoleGuid = Guid.NewGuid();
            Assert.AreEqual(2, _receivedEvents.Count);

            CheckRemove(_testedEntity.StateTemplates, newItem);
        }

        [Test]
        public void Goals_PropertyChanged_IsNotified()
        {
            var newItem = new Goal("Test");

            CheckAdd(_testedEntity.Goals, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.AssociatedStateName = Guid.NewGuid();
            newItem.Script = "AnotherScript";
            Assert.AreEqual(3, _receivedEvents.Count);

            CheckRemove(_testedEntity.Goals, newItem);
        }

        [Test]
        public void Protocols_PropertyChanged_IsNotified()
        {
            var newItem = new Protocol("Test");

            CheckAdd(_testedEntity.Protocols, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.Client = Guid.NewGuid();
            newItem.Server = Guid.NewGuid();
            Assert.AreEqual(3, _receivedEvents.Count);

            CheckRemove(_testedEntity.Protocols, newItem);
        }

        [Test]
        public void Sensors_PropertyChanged_IsNotified()
        {
            var newItem = new Sensor("Test");

            CheckAdd(_testedEntity.Sensors, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.AssociatedAgentInstanceGuid = Guid.NewGuid();
            newItem.Script = "AnotherScript";
            newItem.TargetEnvironment = "AnotherScript";
            Assert.AreEqual(4, _receivedEvents.Count);

            CheckRemove(_testedEntity.Sensors, newItem);
        }

        [Test]
        public void Agents_PropertyChanged_IsNotified()
        {
            var newItem = new AgentRole("Test");

            CheckAdd(_testedEntity.AgentRoles, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Role = "Test2";
            Assert.AreEqual(1, _receivedEvents.Count);

            CheckRemove(_testedEntity.AgentRoles, newItem);
        }

        [Test]
        public void Plans_PropertyChanged_IsNotified()
        {
            var newItem = new Plan("Test");

            CheckAdd(_testedEntity.Plans, newItem);

            //CheckAll Attrubutes
            _receivedEvents.Clear();
            newItem.Name = "Test2";
            newItem.AssociatedAgentGuid = Guid.NewGuid();
            newItem.Goal = new Goal("TestGoal");
            Assert.AreEqual(3, _receivedEvents.Count);

            CheckRemove(_testedEntity.Plans, newItem);
        }

        [Test, TestCaseSource(typeof(DataHolderActions), "AddToHolderActions")]
        public void Equals_ForSameActionsUnderDataHolders_Test(Action<DataHolder, string> addToHolderAction)
        {
            var anotherHolder = new DataHolder();

            Assert.IsTrue(_testedEntity.Equals(anotherHolder));
            addToHolderAction(anotherHolder, "t");
            Assert.IsFalse(_testedEntity.Equals(anotherHolder));
            addToHolderAction(_testedEntity, "t");
            Assert.IsTrue(_testedEntity.Equals(anotherHolder));

            addToHolderAction(anotherHolder, "t1");
            addToHolderAction(_testedEntity, "t2");
            Assert.IsFalse(_testedEntity.Equals(anotherHolder));

            addToHolderAction(anotherHolder, "t2");
            addToHolderAction(_testedEntity, "t1");
            Assert.IsTrue(_testedEntity.Equals(anotherHolder));
        }

        [Test]
        public void ClearData_NotifiersWorking_test()
        {
            //Arrange

            //Act
            _testedEntity.ClearData();
            _receivedEvents.Clear();
            _testedEntity.Environment.Add("t","t");

            //Assert
            Assert.AreEqual(1, _receivedEvents.Count, "Notifiers stoped working");
        }
    }
}