﻿using System;
using Drawer.Data;
using Drawer.Data.Entities;

namespace UnitTestProject.Data
{
    public class DataHolderActions
    {
        public static readonly Action<DataHolder, string>[] AddToHolderActions = {
            (dataHolder, name) =>{dataHolder.Environment.Add(name, name);},
            (dataHolder, name) =>{dataHolder.BeliefsTemplates.Add(new BeliefsTemplate(name));},
            (dataHolder, name) =>
            {
                var b = new BeliefsTemplate(name);
                b.Add(name, name);
                dataHolder.BeliefsTemplates.Add(b);
            },
            (dataHolder, name) =>{dataHolder.StateTemplates.Add(new StateTemplate(name));},
            (dataHolder, name) =>
            {
                var b = new BeliefsTemplate(name);
                b.Add(name, name);
                dataHolder.StateTemplates.Add(new StateTemplate(name));
            },
            (dataHolder, name) =>{dataHolder.Goals.Add(new Goal(name));},
            (dataHolder, name) =>{dataHolder.Plans.Add(new Plan(name));},
            (dataHolder, name) =>{dataHolder.Protocols.Add(new Protocol(name));},
            (dataHolder, name) =>{dataHolder.Sensors.Add(new Sensor(name));},
            (dataHolder, name) =>{dataHolder.AgentRoles.Add(new AgentRole(name));},
        }; 
    }
}