﻿using Drawer.Presentation.Dialogs;
using NUnit.Framework;
using Microsoft.Win32;

namespace UnitTestProject.Application.DialogServices
{
    [TestFixture]
    public class DefaultDialogService_TestClass
    {
        [Test]
        public void OpenFileDoalig_Creation_Check()
        {
            var d = new DefaultDialogsService();
            OpenFileDialog od = d.OpenFileDialog();
        }

        [Test]
        public void SaveFileDoalig_Creation_Check()
        {
            var d = new DefaultDialogsService();
            SaveFileDialog od = d.SaveFileDialog();
        }

    }
}