﻿using System;
using System.Linq;
using System.Windows.Navigation;
using Drawer.CustomTypes.VertexTypes;
using Drawer.Data;
using Drawer.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuickGraph;

namespace UnitTestProject.Application.GraphGenerator
{
    public class DataSingleChangeGraphTestData
    {
        public Action<DataHolder> DataHolderAction;
        public Action<BidirectionalGraph<object, IEdge<object>>> GraphCheck;

        public DataSingleChangeGraphTestData(Action<DataHolder> dataHolderAction,
            Action<BidirectionalGraph<object, IEdge<object>>> graphCheck)
        {
            DataHolderAction = dataHolderAction;
            GraphCheck = graphCheck;
        }
    }

    public class DataChangedGraphTestCaseSource
    {
        //compareByStringAttribute compares the desirable attribute of BaseVertex with the value "t"
        //baseVertexDepndendAttribsGuidComparer compares the desirable guid attribute of main object 
        //--with the guid of dependent object attribute
        static private void CheckConnectionInGraph(BidirectionalGraph<object, IEdge<object>> graph, 
            Func<BaseVertex, string, bool> compareByStringAttribute,
            Func<BaseVertex, BaseVertex, bool> compareByGuidAttribtes
            )
        {
            BaseVertex mainObject = null;
            BaseVertex dependentObject = null;

            foreach (var vertex in graph.Vertices)
            {
                //if (((BaseVertex)vertex).GoalName == "t")
                if (compareByStringAttribute((BaseVertex)vertex, "t"))
                    mainObject = (BaseVertex)vertex;
            }
            Assert.IsNotNull(mainObject);

            foreach (var vertex in graph.Vertices)
            {
                //if (((BaseVertex)vertex).AgentRoleGuid == goal.AssociatedStateName)
                if (compareByGuidAttribtes((BaseVertex)vertex, mainObject))
                    dependentObject = (BaseVertex)vertex;
            }
            Assert.IsNotNull(dependentObject);

            bool searchForEdge = false;
            foreach (var edge in graph.Edges)
            {
                if (edge.Source == dependentObject && edge.Target == mainObject)
                    searchForEdge = true;
            }
            Assert.IsTrue(searchForEdge);
        }

        public static readonly Guid TestGuid = Guid.NewGuid();

        public static DataSingleChangeGraphTestData[] DataSingleChangeGraphTestDataCases =
        {   
            #region Environment
            new DataSingleChangeGraphTestData(
                data => { data.Environment.Add("t","t");},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.EnvironemntAttributeName == "t");
                    Assert.IsTrue(a.EnvironemntAttributeType == "t");
                }
            ),
            #endregion

            #region Beliefs
            new DataSingleChangeGraphTestData(
                data => { data.BeliefsTemplates.Add(new BeliefsTemplate("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.BeliefsTemplateName == "t");
                }
            ),
            new DataSingleChangeGraphTestData(
                data => {                
                    var b = new BeliefsTemplate("t");
                    var agent = new AgentRole("r") {Guid = TestGuid};
                    b.AssociatedAgentRoleGuid = TestGuid;
                    data.BeliefsTemplates.Add(b);
                    data.AgentRoles.Add(agent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.BeliefsTemplateName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedWithBeliefAgentGuid
                        );
                }
            ),
                new DataSingleChangeGraphTestData(
                data => {                
                    var b = new BeliefsTemplate("t");
                    b.Add("t", "t");
                    data.BeliefsTemplates.Add(b);
                },
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.BeliefsAttribs.First().Name == "t");
                    Assert.IsTrue(a.BeliefsAttribs.First().Type == "t");
                }
            ),
            #endregion

            #region States
            new DataSingleChangeGraphTestData(
                data => { data.StateTemplates.Add(new StateTemplate("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.StateName == "t");
                }
            ),
            new DataSingleChangeGraphTestData(
                data => {                
                    var b = new StateTemplate("t");
                    var agent = new AgentRole("r") {Guid = TestGuid};
                    b.AssociatedAgentRoleGuid = TestGuid;
                    data.StateTemplates.Add(b);
                    data.AgentRoles.Add(agent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.StateName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedWithStateAgentGuid
                        );
                }
            ),
                new DataSingleChangeGraphTestData(
                data => {                
                    var b = new StateTemplate("t");
                    b.Add("t", "t");
                    data.StateTemplates.Add(b);
                },
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.StateAttribs.First().Name == "t");
                    Assert.IsTrue(a.StateAttribs.First().Type == "t");
                }
            ),
            #endregion

            #region AgentRole
            new DataSingleChangeGraphTestData(
                data => { data.AgentRoles.Add(new AgentRole("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.AgentRoleName == "t");
                }
            ),
            #endregion

            #region Goal
            new DataSingleChangeGraphTestData(
                data => { data.Goals.Add(new Goal("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.GoalName == "t");
                }
            ),
            new DataSingleChangeGraphTestData(
                data =>
                {
                    var newGoal = new Goal("t");
                    var newAgent = new AgentRole("t") {Guid = TestGuid};
                    newGoal.AssociatedStateName = TestGuid;
                    data.Goals.Add(newGoal);
                    data.AgentRoles.Add(newAgent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.GoalName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedStateName
                        );
                }
            ),
            #endregion

            #region Plan
            new DataSingleChangeGraphTestData(
                data => { data.Plans.Add(new Plan("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.PlanName == "t");
                }
            ),
            #endregion

            #region Protocol
            new DataSingleChangeGraphTestData(
                data =>
                {
                    data.Protocols.Add(new Protocol("t"));
                },
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.ProtocolName == "t");
                }
            ),
            new DataSingleChangeGraphTestData(
                data =>
                {
                    var newProtocol = new Protocol("t");
                    var newAgent = new AgentRole("t") {Guid = TestGuid};
                    newProtocol.Client = TestGuid;
                    data.Protocols.Add(newProtocol);
                    data.AgentRoles.Add(newAgent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.ProtocolName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedClientAgentGuidToThisProtocol
                        );
                }
            ),
            new DataSingleChangeGraphTestData(
                data =>
                {
                    var newProtocol = new Protocol("t");
                    var newAgent = new AgentRole("t") {Guid = TestGuid};
                    newProtocol.Server = TestGuid;
                    data.Protocols.Add(newProtocol);
                    data.AgentRoles.Add(newAgent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.ProtocolName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedServerAgentGuidToThisProtocol
                        );
                }
            ),
            #endregion

            #region Sensor
            new DataSingleChangeGraphTestData(
                data => { data.Sensors.Add(new Sensor("t"));},
                graph =>
                {
                    var a = (BaseVertex)graph.Vertices.First();
                    Assert.IsTrue(a.SensorName == "t");
                }
            ),
            new DataSingleChangeGraphTestData(
                data =>
                {
                    var newSensor = new Sensor("t");
                    var newAgent = new AgentRole("t") {Guid = TestGuid};
                    newSensor.AssociatedAgentInstanceGuid = TestGuid;
                    data.Sensors.Add(newSensor);
                    data.AgentRoles.Add(newAgent);
                },
                graph =>
                {
                    CheckConnectionInGraph(graph,
                        (bv, name) => bv.SensorName == "t",
                        (bv1, bv2) => bv1.AgentRoleGuid == bv2.AssociatedAgentGuidToThisSensor
                        );
                }
            ),
            #endregion
        };
    }
}