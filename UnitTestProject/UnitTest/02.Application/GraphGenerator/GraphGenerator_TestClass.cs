﻿using Drawer.Application;
using Drawer.Data;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.Application.GraphGenerator
{
    [TestFixture]
    public class GraphGenerator_TestClass
    {
        public DataHolder Data;
        private GraphGeneratorService _testedEntity;

        [SetUp]
        public void Init()
        {
            Data = new DataHolder();
            _testedEntity = new GraphGeneratorService();
        }

        [Test, TestCaseSource(typeof (DataChangedGraphTestCaseSource), "DataSingleChangeGraphTestDataCases")]
        public void GenerateGraphToVisualize_ReadingFromData(DataSingleChangeGraphTestData testDataCaseDataSingle)
        {
            testDataCaseDataSingle.DataHolderAction(Data);

            var graph = _testedEntity.GenerateGraphToVisualize(Data);

            testDataCaseDataSingle.GraphCheck(graph);
        }

        [Test]
        public void GenerateeGraphToVisualize_ConnectionBetweenNewAttribAndSensor_IsPresent_Bug()
        {
            //Arrange
            Data.Environment.Add("t", "t");
            Data.Sensors.Add(new Sensor("t"));

            //Act
            var graph = _testedEntity.GenerateGraphToVisualize(Data);

            //Assert
            Assert.IsTrue(graph.EdgeCount == 0, "Wrong edge was creared!");
        }
    }
}