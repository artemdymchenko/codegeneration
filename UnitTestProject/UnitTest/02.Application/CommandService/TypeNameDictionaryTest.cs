﻿using System.Collections.ObjectModel;
using Drawer.Application.CommandsService;
using Drawer.Data.Entities;
using NUnit.Framework;

namespace UnitTestProject.UnitTest.Application.CommandService
{
    public class TypeNameDictionaryTest
    {
        public static TypeNameDictionaryTest[] TypeNameDictionaryAddTestCases =
        {
            new TypeNameDictionaryTest(false, "TestAttr1", "TestAttrType1", "", "", 2, true),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr1", "TestAttrType1", 1, false),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr1", "TestAttrType2", 1, false),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr2", "TestAttrType2", 2, true),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr2", "TestAttrType1", 2, true),
        };

        public static TypeNameDictionaryTest[] TypeNameDictionaryRemoveTestCases =
        {
            new TypeNameDictionaryTest(false, "TestAttr1", "TestAttrType1", "", "", 1, false),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr1", "", 0, true),
            new TypeNameDictionaryTest(true, "TestAttr1", "TestAttrType1", "TestAttr2", "", 1, false),
        };

        public bool CreateEntityBeforeAddingAttribute { get; private set; }
        public string Attr1Name { get; private set; }
        public string Attr1Type { get; private set; }
        public string Attr2Name { get; private set; }
        public string Attr2Type { get; private set; }
        public int ExpectedNumberOfAttribs { get; private set; }
        public bool ExpectedResultOfLastOp { get; private set; }

        public TypeNameDictionaryTest(bool createEntityBeforeAddingAttribute, string attr1Name, string attr1Type, string attr2Name, string attr2Type, int expectedNumberOfAttribs, bool expectedResultOfLastOp)
        {
            this.CreateEntityBeforeAddingAttribute = createEntityBeforeAddingAttribute;
            this.Attr1Name = attr1Name;
            this.Attr1Type = attr1Type;
            this.Attr2Name = attr2Name;
            this.Attr2Type = attr2Type;
            this.ExpectedNumberOfAttribs = expectedNumberOfAttribs;
            this.ExpectedResultOfLastOp = expectedResultOfLastOp;
        }

        public static void AddingToTest<T>(
            BaseParser.ThreeStringsMethodOfBool methodUnderTest, 
            BaseParser.StringMethodOfBool typeNameCreator,
            ObservableCollection<T> targetCollection,
            TypeNameDictionaryTest testSrc) where T : BaseTypeNameDictionary
        {
            var entityName = "Test";
            if (!testSrc.CreateEntityBeforeAddingAttribute)
            {
                Assert.IsFalse(methodUnderTest(entityName, testSrc.Attr1Name, testSrc.Attr1Type));
                return;
            }

            typeNameCreator(entityName);

            Assert.IsTrue(methodUnderTest(entityName, testSrc.Attr1Name, testSrc.Attr1Type));
            Assert.AreEqual(testSrc.ExpectedResultOfLastOp, methodUnderTest(entityName, testSrc.Attr2Name, testSrc.Attr2Type));

            Assert.AreEqual(testSrc.ExpectedNumberOfAttribs, targetCollection[0].Attribs.Count);
        }

        public static void RemovingFromTest<T>(
            BaseParser.TwoStringsMethodOfBool methodUnderTest,
            BaseParser.StringMethodOfBool methodToAddEntity,
            BaseParser.ThreeStringsMethodOfBool methodToAddAttrib,
            ObservableCollection<T> targetCollection,
            TypeNameDictionaryTest testSrc) where T : BaseTypeNameDictionary
        {
            var entityName = "Test";
            if (!testSrc.CreateEntityBeforeAddingAttribute)
            {
                Assert.IsFalse(methodUnderTest(entityName, testSrc.Attr2Name));
                return;
            }
            methodToAddEntity(entityName);
            methodToAddAttrib(entityName, testSrc.Attr1Name, testSrc.Attr1Type);

            var res = methodUnderTest(entityName, testSrc.Attr2Name);

            Assert.AreEqual(testSrc.ExpectedResultOfLastOp, res);
            Assert.AreEqual(testSrc.ExpectedNumberOfAttribs, targetCollection[0].Attribs.Count);
        }
    }

}