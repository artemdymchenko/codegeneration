﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using Drawer.Application.CommandsService;
using NUnit.Framework;

namespace UnitTestProject.UnitTest.Application.CommandService
{
    [TestFixture]
    public class ConsoleCommandParser_TestClass
    {
        private List<string> _receivedEvents;
        private ConsoleCommandParser _testedEntity;

        [SetUp]
        public void Init()
        {
            _testedEntity = new ConsoleCommandParser();
            _receivedEvents = new List<string>();

            #region Set event handling

            _testedEntity.AddEnvironmentAttributeCommand += delegate{_receivedEvents.Add("AddEnvironmentAttributeCommand");return true;};
            _testedEntity.RemoveEnvironmentAttributeCommand += delegate { _receivedEvents.Add("RemoveEnvironmentAttributeCommand"); return true; };

            _testedEntity.AddBeliefsTemplateCommand += delegate { _receivedEvents.Add("AddBeliefsTemplateCommand"); return true; };
            _testedEntity.RemoveBeliefsTemplateCommand += delegate { _receivedEvents.Add("RemoveBeliefsTemplateCommand"); return true; };
            _testedEntity.AddBeliefsAttributeCommand += delegate { _receivedEvents.Add("AddBeliefsAttributeCommand"); return true; };
            _testedEntity.RemoveBeliefsAttributeCommand += delegate { _receivedEvents.Add("RemoveBeliefsAttributeCommand"); return true; };
            _testedEntity.ShowBeliefsTemplatesCommand += delegate { _receivedEvents.Add("ShowBeliefsTemplatesCommand"); return ; };
            _testedEntity.RemoveBeliefsTemplateCommand += delegate { _receivedEvents.Add("RemoveBeliefsTemplateCommand"); return true; };

            _testedEntity.AddStateCommand += delegate { _receivedEvents.Add("AddStateCommand"); return true; };
            _testedEntity.DeleteStateCommand += delegate { _receivedEvents.Add("DeleteStateCommand"); return true; };
            _testedEntity.AddStateAttributeCommand += delegate { _receivedEvents.Add("AddStateAttributeCommand"); return true; };
            _testedEntity.RemoveStateAttributeCommand += delegate { _receivedEvents.Add("RemoveStateAttributeCommand"); return true; };
            _testedEntity.ShowStatesCommand += delegate { _receivedEvents.Add("ShowStatesCommand"); return ; };

            _testedEntity.ConnectStateCommand += delegate { _receivedEvents.Add("ConnectStateCommand"); return true; };
            _testedEntity.DisconnectStateCommand += delegate { _receivedEvents.Add("DisconnectStateCommand"); return true; };
            _testedEntity.AddPlanCommand += delegate { _receivedEvents.Add("AddPlanCommand"); return true; };
            _testedEntity.DeletePlanCommand += delegate { _receivedEvents.Add("DeletePlanCommand"); return true; };
            _testedEntity.ShowPlansCommand += delegate { _receivedEvents.Add("ShowPlansCommand"); return ; };

            _testedEntity.ConnectPlanToAgentCommand += delegate { _receivedEvents.Add("ConnectPlanToAgentCommand"); return true; };
            _testedEntity.DisconnectPlanToAgentCommand += delegate { _receivedEvents.Add("DisconnectPlanToAgentCommand"); return true; };
            _testedEntity.ConnectPlanToGoalCommand += delegate { _receivedEvents.Add("ConnectPlanToGoalCommand"); return true; };
            _testedEntity.DisconnectPlanToGoalCommand += delegate { _receivedEvents.Add("DisconnectPlanToGoalCommand"); return true; };

            _testedEntity.AddGoalCommand += delegate { _receivedEvents.Add("AddGoalCommand"); return true; };
            _testedEntity.DeleteGoalCommand += delegate { _receivedEvents.Add("DeleteGoalCommand"); return true; };
            _testedEntity.ShowGoalsCommand += delegate { _receivedEvents.Add("ShowGoalsCommand"); return ; };

            _testedEntity.ConnectGoalToStateCommand += delegate { _receivedEvents.Add("ConnectGoalToStateCommand"); return true; };
            _testedEntity.DisconnectGoalToAgentCommand += delegate { _receivedEvents.Add("DisconnectGoalFromStateCommand"); return true; };
            _testedEntity.ConnectGoalToPlanCommand += delegate { _receivedEvents.Add("ConnectGoalToPlanCommand"); return true; };
            _testedEntity.DisconnectGoalToPlanCommand += delegate { _receivedEvents.Add("DisconnectGoalToPlanCommand"); return true; };

            _testedEntity.AddProtocolCommand += delegate { _receivedEvents.Add("AddProtocolCommand"); return true; };
            _testedEntity.DeleteProtocolCommand += delegate { _receivedEvents.Add("DeleteProtocolCommand"); return true; };
            _testedEntity.AddMethodToProtocolCommand += delegate { _receivedEvents.Add("AddMethodToProtocolCommand"); return true; };
            _testedEntity.RemoveMethodToProtocolCommand += delegate { _receivedEvents.Add("RemoveMethodToProtocolCommand"); return true; };

            _testedEntity.AddClientAgentToProtocolCommand += delegate { _receivedEvents.Add("AddClientAgentToProtocolCommand"); return true; };
            _testedEntity.RemoveClientAgentFromProtocolCommand += delegate { _receivedEvents.Add("RemoveClientAgentFromProtocolCommand"); return true; };
            _testedEntity.AddServerAgentToProtocolCommand += delegate { _receivedEvents.Add("AddServerAgentToProtocolCommand"); return true; };
            _testedEntity.RemoveServerAgentFromProtocolCommand += delegate { _receivedEvents.Add("RemoveServerAgentFromProtocolCommand  "); return true; };

            _testedEntity.AddSensorCommand += delegate { _receivedEvents.Add("AddSensorCommand"); return true; };
            _testedEntity.RemoveSensorCommand += delegate { _receivedEvents.Add("RemoveSensorCommand"); return true; };
            _testedEntity.AddSensorScriptCommand += delegate { _receivedEvents.Add("AddSensorScriptCommand"); return true; };
            _testedEntity.RemoveSensorScriptCommand += delegate { _receivedEvents.Add("RemoveSensorScriptCommand"); return true; };
            _testedEntity.AddSensorListeningCommand += delegate { _receivedEvents.Add("AddSensorListeningCommand"); return true; };
            _testedEntity.RemoveSensorListeningCommand += delegate { _receivedEvents.Add("RemoveSensorListeningCommand"); return true; };
            _testedEntity.AddSensorToAgentCommand += delegate { _receivedEvents.Add("AddSensorToAgentCommand"); return true; };
            _testedEntity.RemoveSensorFromAgentCommand += delegate { _receivedEvents.Add("RemoveSensorFromAgentCommand"); return true; };

            _testedEntity.AddAgentCommand += delegate { _receivedEvents.Add("DisconnectGoalFromStateCommand"); return true; };
            _testedEntity.RemoveAgentCommand += delegate { _receivedEvents.Add("DisconnectGoalFromStateCommand"); return true; };

            _testedEntity.SaveDocumentCommand += delegate { _receivedEvents.Add("SaveDocumentCommand"); return ; };
            _testedEntity.LoadDocumentCommand += delegate { _receivedEvents.Add("LoadDocumentCommand"); return ; };
            _testedEntity.ClearDataCommand += delegate { _receivedEvents.Add("ClearDataCommand"); return ; };
            _testedEntity.GenerateXMLCommand += delegate { _receivedEvents.Add("GenerateXMLCommand"); return ; };

            #endregion
        }

        [TestCase("add_attr int a", ConsoleCommandParser.operationSucceed, "AddEnvironmentAttributeCommand")]
        [TestCase("add_attr int ", ConsoleCommandParser.wrongNumberOfAttributes, "AddEnvironmentAttributeCommand")]
        [TestCase("add_attr int a r", ConsoleCommandParser.wrongNumberOfAttributes, "AddEnvironmentAttributeCommand")]
        [TestCase("add_attr ii t", ConsoleCommandParser.unknownAttributeType, "AddEnvironmentAttributeCommand")]

        public void ParseCommand_TestAllCases(string commandInput, string expectedResult, string expectedCallbackMessage)
        {
            var command = commandInput.Split(' ');
            command[0] = ">" + command[0];

            var res = _testedEntity.ParseCommand(command);
            bool errorExpected = expectedResult != ConsoleCommandParser.operationSucceed;

            Assert.AreEqual(expectedResult, res);
            Assert.IsTrue(_receivedEvents.Count == (errorExpected ? 0 : 1));

            if (!errorExpected)
                Assert.AreEqual(_receivedEvents[0], expectedCallbackMessage);
        }

        //Itegrational test - add two environment attribs
        //public voi
    }
}