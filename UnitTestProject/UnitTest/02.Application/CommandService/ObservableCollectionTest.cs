﻿using System.Collections.ObjectModel;
using Drawer.Application.CommandsService;
using NUnit.Framework;

namespace UnitTestProject.UnitTest.Application.CommandService
{
    public class ObservableCollectionTest
    {
        static readonly ObservableCollectionTest[] AddToTestCases =
        {
            new ObservableCollectionTest("Test1", "Test1", 1, false),
            new ObservableCollectionTest("Test1", "Test2", 2, true),
        };

        static readonly ObservableCollectionTest[] RemoveFromTestCases =
        {
            new ObservableCollectionTest("Test1", "Test1", 0, true),
            new ObservableCollectionTest("Test1", "Test2", 1, false),
        };

        public string Entity1Name { get; private set; }
        public string Entity2Name { get; private set; }
        public int ExpectedNumberOfEntities { get; private set; }
        public bool ExpectedResultOfAction { get; private set; }

        public ObservableCollectionTest(string entity1Name, string entity2Name, int expectedNumberOfEntities, bool expectedResultOfAction)
        {
            this.Entity1Name = entity1Name;
            this.Entity2Name = entity2Name;
            this.ExpectedResultOfAction = expectedResultOfAction;
            this.ExpectedNumberOfEntities = expectedNumberOfEntities;
        }

        public static void AddingToTest<T>(
            BaseParser.StringMethodOfBool methodUnderTest, ObservableCollection<T> targetCollection,
            ObservableCollectionTest testSrc)
        {
            methodUnderTest(testSrc.Entity1Name);
            bool res = methodUnderTest(testSrc.Entity2Name);

            Assert.AreEqual(testSrc.ExpectedResultOfAction, res);
            Assert.AreEqual(testSrc.ExpectedNumberOfEntities, targetCollection.Count);
        }

        public static void RemovingFromTest<T>(
            BaseParser.StringMethodOfBool removeMethodUnderTest, BaseParser.StringMethodOfBool addMethod,
            ObservableCollection<T> targetCollection,
            ObservableCollectionTest testSrc)
        {
            addMethod(testSrc.Entity1Name);
            bool res = removeMethodUnderTest(testSrc.Entity2Name);

            Assert.AreEqual(testSrc.ExpectedResultOfAction, res);
            Assert.AreEqual(testSrc.ExpectedNumberOfEntities, targetCollection.Count);
        }
    }
}