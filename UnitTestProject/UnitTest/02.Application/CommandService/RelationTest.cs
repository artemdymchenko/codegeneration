﻿using Drawer.Application.CommandsService;
using NUnit.Framework;

namespace UnitTestProject.UnitTest.Application.CommandService
{
    public class RelationTest
    {
        public static RelationTest[] ConjunctionTestCaseData =
        {
            new RelationTest(true, true, true),
            new RelationTest(false, true, false),
            new RelationTest(true, false, false),
            new RelationTest(false, false, false),
        };

        public static RelationTest[] SpecialTestCaseData =
        {
            new RelationTest(true, true, true),
            new RelationTest(false, true, false),
            new RelationTest(true, false, true),
            new RelationTest(false, false, false),
        };

        public bool ExpectedResult { get; set; }
        public bool CreateFirstEntity { get; set; }
        public bool CreateSecondEntity { get; set; }

        public RelationTest(bool createFirstEntity, bool createSecondEntity, bool expectedResult)
        {
            ExpectedResult = expectedResult;
            CreateFirstEntity = createFirstEntity;
            CreateSecondEntity = createSecondEntity;
        }

        public static void SetRelationBetweenTwoEntites(
            BaseParser.StringMethodOfBool entity1Creator,
            BaseParser.StringMethodOfBool entity2Creator,
            BaseParser.TwoStringsMethodOfBool testedMethod,
            bool firstParameterCreated,
            bool secondParameterCreated,
            bool expectedResult
        )
        {
            var name1 = "TestName1";
            var name2 = "TestName2";

            if (firstParameterCreated)
                entity1Creator(name1);

            if (secondParameterCreated)
                entity2Creator(name2);

            Assert.AreEqual(expectedResult, testedMethod(name1, name2));
        }
    }
}