﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.ComTypes;
using Drawer.Application.CommandsService;
using Drawer.Data;
using Drawer.Data.Entities;
using Drawer.XMLGenerator;
using Moq;
using NUnit.Framework;

namespace UnitTestProject.UnitTest.Application.CommandService
{
    [TestFixture]
    public class CommandService_testClass
    {
        private Drawer.Application.CommandsService.CommandService _testedEntity;

        [SetUp]
        public void Init()
        {
            var dataHolder = new DataHolder();
            var xmlGenerator = Mock.Of<IXMLGenerator>();
            var parser = Mock.Of<BaseParser>();
            _testedEntity = new Drawer.Application.CommandsService.CommandService(dataHolder, xmlGenerator, parser);
        }

        #region AgentTest
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddAgentTest_NumberOfAddedAgents(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddAgentCommand,
                _testedEntity.DataInstance.AgentRoles, 
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveAgentTest_NumberOfAddedAgents(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveAgentCommand, 
                _testedEntity.AddAgentCommand,
                _testedEntity.DataInstance.AgentRoles,
                testSrc);
        }
        #endregion

        #region BeliefsTest

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddBeliefsTest_NumberOfAddedBeliefss(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.DataInstance.BeliefsTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveBeliefsTest_NumberOfAddedBeliefss(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveBeliefsTemplateCommand,
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.DataInstance.BeliefsTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryAddTestCases")]
        public void AddBeliefsAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            TypeNameDictionaryTest.AddingToTest(
                _testedEntity.AddBeliefsAttributeCommand, 
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.DataInstance.BeliefsTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryRemoveTestCases")]
        public void RemoveBeliefsAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            TypeNameDictionaryTest.RemovingFromTest(
                _testedEntity.RemoveBeliefsAttributeCommand,
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.AddBeliefsAttributeCommand,
                _testedEntity.DataInstance.BeliefsTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectBeliefsCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectBeliefsCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "SpecialTestCaseData")]
        public void DisonnectBeliefsCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddBeliefsTemplateCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectBeliefsCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }
        #endregion

        #region Environment

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryAddTestCases")]
        public void AddEnvironmentAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            Assert.IsTrue(_testedEntity.AddEnvironmentAttributeCommand(testSrc.Attr1Name, testSrc.Attr1Type));
            Assert.AreEqual(testSrc.ExpectedResultOfLastOp, _testedEntity.AddEnvironmentAttributeCommand(testSrc.Attr2Name, testSrc.Attr2Type));
            Assert.AreEqual(testSrc.ExpectedNumberOfAttribs, _testedEntity.DataInstance.Environment.Attribs.Count);
        }

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryRemoveTestCases")]
        public void RemoveEnvironmentAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            _testedEntity.AddEnvironmentAttributeCommand(testSrc.Attr1Name, testSrc.Attr1Type);
            var res = _testedEntity.RemoveEnvironmentAttributeCommand(testSrc.Attr2Name);
            Assert.AreEqual(testSrc.ExpectedResultOfLastOp, res);
            Assert.AreEqual(testSrc.ExpectedNumberOfAttribs, _testedEntity.DataInstance.Environment.Attribs.Count);
        }
        #endregion

        #region Goal

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddGoalCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddGoalCommand,
                _testedEntity.DataInstance.Goals,
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveGoalCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveGoalCommand,
                _testedEntity.AddGoalCommand,
                _testedEntity.DataInstance.Goals,
                testSrc);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectGoalToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddGoalCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectGoalToStateCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "SpecialTestCaseData")]
        public void DisconnectGoalToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddGoalCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectGoalFromStateCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectGoalToPlanCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddGoalCommand,
                _testedEntity.AddPlanCommand,
                _testedEntity.ConnectGoalToPlanCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        //ConjunctionTestCaseData is passed as a sourceCaseData on purpose
        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void DisconnectGoalToPlanCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddGoalCommand,
                _testedEntity.AddPlanCommand,
                _testedEntity.DisconnectGoalToPlanCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        #endregion

        #region Plan

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddPlanCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddPlanCommand,
                _testedEntity.DataInstance.Plans,
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemovePlanCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemovePlanCommand,
                _testedEntity.AddPlanCommand,
                _testedEntity.DataInstance.Plans,
                testSrc);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectPlanToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddPlanCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectPlanToAgentCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }


        [Test, TestCaseSource(typeof(RelationTest), "SpecialTestCaseData")]
        public void DisconnectPlanToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddPlanCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectPlanToAgentCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }


        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectPlanToGoalCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddPlanCommand,
                _testedEntity.AddGoalCommand,
                _testedEntity.ConnectPlanToGoalCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        //ConjunctionTestCaseData is passed as a sourceCaseData on purpose
        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void DisconnectPlanFromGoalCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddPlanCommand,
                _testedEntity.AddGoalCommand,
                _testedEntity.DisconnectPlanToGoalCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }
        #endregion

        #region Protocol

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddProtocolCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddProtocolCommand,
                _testedEntity.DataInstance.Protocols,
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveProtocolCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveProtocolCommand,
                _testedEntity.AddProtocolCommand,
                _testedEntity.DataInstance.Protocols,
                testSrc);
        }


        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectCientAgentToProtocolCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddProtocolCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectClientAgentToProtocolCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void DisconnectProtocolToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddProtocolCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectClientAgentFromProtocolCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }


        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectServerAgentToProtocolCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddProtocolCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectServerAgentToProtocolCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void DisconnectProtocolToProtocolCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddProtocolCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectServerAgentFromProtocolCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        #endregion

        #region Sensor

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddSensorCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddSensorCommand,
                _testedEntity.DataInstance.Sensors,
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveSensorCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveSensorCommand,
                _testedEntity.AddSensorCommand,
                _testedEntity.DataInstance.Sensors,
                testSrc);
        }

        [Test]
        public void AddSensorToScriptCommand_Test()
        {
            //TODO:
        }

        [Test]
        public void RemoveSensorScriptCommand_Test()
        {
            //TODO:
        }

        [Test]
        public void AddSensorListeningCommand_Test()
        {
            //Arrange

            //Act
            _testedEntity.AddSensorCommand("Eye");
            _testedEntity.AddEnvironmentAttributeCommand("Name", "bool");

            //Assert
            Assert.IsTrue(_testedEntity.AddSensorListeningCommand("Eye", "Name"), "Failed to connect sensor to attribute");
        }

        [Test]
        public void AddSensorListeningCommand_Serialization_Bug()
        {
            //Arrange

            //Act
            _testedEntity.AddSensorCommand("Eye");
            _testedEntity.AddEnvironmentAttributeCommand("Name", "bool");

            //Assert
            Assert.IsTrue(_testedEntity.AddSensorListeningCommand("Eye", "Name"), "Failed to connect sensor to attribute");
        }

        [Test]
        public void RemoveSensorListeningCommand_Test()
        {
            //TODO:
        }

        [Test]
        public void AddSensorToAgentCommand_Test()
        {
            //TODO:
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void AddSensorToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddSensorCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectSensorToAgentCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void RemoveSensorFromAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddSensorCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectSensorFromAgentCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }
        #endregion

        #region State

        [Test, TestCaseSource(typeof(ObservableCollectionTest), "AddToTestCases")]
        public void AddStateCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.AddingToTest(
                _testedEntity.AddStateCommand,
                _testedEntity.DataInstance.StateTemplates,
                testSrc);
        }
        [Test, TestCaseSource(typeof(ObservableCollectionTest), "RemoveFromTestCases")]
        public void RemoveStateCommand_Test(ObservableCollectionTest testSrc)
        {
            ObservableCollectionTest.RemovingFromTest(
                _testedEntity.RemoveStateCommand,
                _testedEntity.AddStateCommand,
                _testedEntity.DataInstance.StateTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryAddTestCases")]
        public void AddStateAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            TypeNameDictionaryTest.AddingToTest(
                _testedEntity.AddStateAttributeCommand, 
                _testedEntity.AddStateCommand,
                _testedEntity.DataInstance.StateTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(TypeNameDictionaryTest), "TypeNameDictionaryRemoveTestCases")]
        public void RemoveStateAttributeCommand_Test(TypeNameDictionaryTest testSrc)
        {
            TypeNameDictionaryTest.RemovingFromTest(
                _testedEntity.RemoveStateAttributeCommand,
                _testedEntity.AddStateCommand,
                _testedEntity.AddStateAttributeCommand,
                _testedEntity.DataInstance.StateTemplates,
                testSrc);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void ConnectStateToAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddStateCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.ConnectStateCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }

        [Test, TestCaseSource(typeof(RelationTest), "ConjunctionTestCaseData")]
        public void RemoveStateFromAgentCommand_Test(RelationTest testSrc)
        {
            RelationTest.SetRelationBetweenTwoEntites(
                _testedEntity.AddStateCommand,
                _testedEntity.AddAgentCommand,
                _testedEntity.DisconnectStateCommand,
                testSrc.CreateFirstEntity,
                testSrc.CreateSecondEntity,
                testSrc.ExpectedResult);
        }
        #endregion
    }
}