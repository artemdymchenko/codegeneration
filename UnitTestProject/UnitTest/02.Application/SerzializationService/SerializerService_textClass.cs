﻿using System;
using Drawer;
using Drawer.Data;
using NUnit.Framework;
using UnitTestProject.Data;

namespace UnitTestProject.Application.SerializationService
{
    [TestFixture]
    public class SerializerService_TestClass
    {
        private SerializerService _testedEntity;
        public const string FileName = "serializationTest";

        [SetUp]
        public void Init()
        {
            _testedEntity = new SerializerService();
        }

        public void TestSerialization(DataHolder etalon, DataHolder modified, bool expectedResult)
        {
            try
            {
                _testedEntity.SerializeObject(modified, FileName);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }

            try
            {
                modified.UpdateData(_testedEntity.DeSerializeObject<DataHolder>(FileName));
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
            Assert.AreEqual(expectedResult, etalon.Equals(modified));
        }

        [Test, TestCaseSource(typeof(DataHolderActions), "AddToHolderActions")]
        public void SerializeAndDeserialize_Test(Action<DataHolder, string> addToHolderAction)
        {
            var etalon = new DataHolder();
            var another = new DataHolder();

            TestSerialization(etalon, another, true);
            addToHolderAction(another, "t");
            TestSerialization(etalon, another, false);
            addToHolderAction(etalon, "t");
            TestSerialization(etalon, another, true);

            addToHolderAction(another, "t1");
            addToHolderAction(etalon, "t2");
            TestSerialization(etalon, another, false);

            addToHolderAction(another, "t2");
            addToHolderAction(etalon, "t1");
            TestSerialization(etalon, another, true);
        }

        [Test]
        public void EnvironmentAttribute_IsMisingAfterDeserialization_Bug()
        {
            var dataHolder = new DataHolder();
            dataHolder.Environment.Add("t", "n");
            try
            {
                _testedEntity.SerializeObject(dataHolder, FileName);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }

            try
            {
                dataHolder.UpdateData(_testedEntity.DeSerializeObject<DataHolder>(FileName));
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }

            Assert.IsTrue(dataHolder.Environment.ContainsKey("n"), "EnvironmentAttribute name and type swaped after serialization");
        }
    }
}